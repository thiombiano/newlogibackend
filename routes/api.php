<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => 'api'
], function ($router) {
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::apiResource('/typechantiers', 'TypeChantierController');

Route::apiResource('/fonctions', 'FonctionController');

Route::apiResource('/modepaiments', 'ModepaiementController');

Route::apiResource('/lots', 'LotController');

Route::apiResource('/etapes', 'EtapeconstructionController');

Route::apiResource('/imageetapeencour', 'ImageEtapeController');

Route::apiResource('/users', 'UserController');

Route::get('/test', 'FournisseurController@rp');

Route::get('/stocks/getquantitedisponible/{param1}/{param2}', 'StockController@getQuantiteDisponibleByIDarticleAndIDdepot');

Route::get('/besoins/alldetailbesoin/{param1}', 'BesoinJournalierController@getalldetailbesoinlevelone');

Route::get('/besoins/alldetailbesoininprefat/{param1}', 'BesoinJournalierController@getalldetailbesoininprefatlevelone');

Route::get('/besoinsbytechnicien/{param1}/{param2}', 'BesoinJournalierController@indexbesionsbyutilisateur');

Route::get('/enlevementsbymagasinier/{param1}/{param2}', 'EnlevementController@indexenlevementsbyutilisateur');

Route::get('/besoins/alldetailbesoinbymaison/{param1}/{param2}', 'BesoinJournalierController@getalldetailbesoinbymaisonleveltwo');

Route::get('/besoins/alldetailbesoinbyprefat/{param1}/{param2}', 'BesoinJournalierController@getalldetailbesoinbyprefatleveltwo');

Route::get('/fonctions/getfonctionbyidmaison/{param1}', 'FonctionController@getlistefonctionbyidmaison');

Route::get('/fonctions/getfonctionbyidprefat/{param1}', 'FonctionController@getlistefonctionbyidprefat');

Route::get('/fonctions/getprestatairebyidmaisonandidfonction/{param1}/{param2}', 'FonctionController@getlisteprestatairebyidmaisonandidfonction');

Route::get('/fonctions/getprestatairebyidprefatandidfonction/{param1}/{param2}', 'FonctionController@getlisteprestatairebyidprefatandidfonction');

Route::get('/userstechniciens', 'UserController@getusertechnicien');

Route::get('/usersmagasiniers', 'UserController@getusermagasinier');

Route::get('/userscontroleurs', 'UserController@getusercontroleur');

Route::get('/lotsbysections/{param1}', 'SectionController@getlotbysection');

Route::get('/lotstoaddhome/{param1}', 'LotController@getlottoaddhome');

Route::get('/maisonbysections/{param1}', 'LotController@showmaisonbysection');

Route::get('/maisonbychantiers/{param1}', 'LotController@showmaisonbychantier');

Route::get('/maisonbyid/{param1}', 'MaisonController@showmaisonbymaisonid');

Route::get('/prefatbyid/{param1}', 'PrefatController@showprefatbyid');

Route::get('/getetapebymaison/{param1}', 'EtapeConstructionController@getetapebymaison');

Route::get('/getetapebytypemaison/{param1}', 'EtapeConstructionController@getetapebytypemaison');

Route::get('/getetapedisponiblebymaison/{param1}', 'EtapeConstructionController@getetapedisponibleformaison');

Route::get('/employebyfonction/{param1}', 'EmployeController@showemployebyfonction');

Route::get('/prestationbymaison/{param1}', 'PrestationController@showprestationbymaison');

Route::get('/prestationbyprefat/{param1}', 'PrestationController@showprestationbyprefat');

Route::get('/chantiers/{param1}/receptionpartiellefinale', 'ReceptionController@displayallreceptionpartiellefinale');

Route::get('/chantiers/{param1}/receptiontotale', 'ReceptionController@displayallReceptiontotale');

Route::get('/chantiers/{param1}/receptionpartielle', 'ReceptionController@displayallReceptionpartielle');

Route::get('/chantiers/{param1}/todaybesionjournalier', 'BesoinJournalierController@showtodatbesoinjournalier');

Route::get('/listefournisseurnonblacklister', 'FournisseurController@listedesfournisseurnonblacklister');

Route::get('/chantiers/{param1}/touslesbesoinsjournaliersaujourdhui', 'BesoinJournalierController@touslesbesoinsjournaliersitems');

Route::apiResource('/audits', 'AuditController');

Route::apiResource('/etapemaisonencour', 'EtapestapeinmaisonController');

Route::apiResource('/livreurs', 'LivreurController');

Route::apiResource('/chantiers', 'ChantierController');

Route::apiResource('/prestations', 'PrestationController');

Route::apiResource('/typecontrats', 'TypecontratController');

Route::post('/chantiers/{idchantier}/enlevementforhomedirectly', 'EnlevementController@storeForEnlevementDirectVersMaison');

Route::post('/chantiers/{idchantier}/enlevementforprefatdirectly', 'EnlevementController@storeEnlevementDirectlyOnPrefat');

Route::post('/annulationboncommande/{idbon}', 'ChantierCommandesController@updateforbonannulation');

Route::post('/receptionneetapeencour/', 'EtapestapeinmaisonController@ReceptionneEtape');

Route::post('/receptionnedefinitivementetapeencour/', 'EtapestapeinmaisonController@ReceptionneDefinitivementEtape');

Route::post('/annulerreceptionetapeencour/', 'EtapestapeinmaisonController@AnnulerReceptionEtape');

Route::post('/blacklisterfournisseur/{idfournisseur}', 'FournisseurController@blacklisterfournisseur');

Route::post('/autoriserfournisseur/{idfournisseur}', 'FournisseurController@reactiverfournisseur');

Route::post('/annulationrecueilbesoin/{idbesoin}', 'RecueilBesoinController@updateforbesoinannulation');

Route::get('/validerrecueilbesoin/{idbesoin}', 'RecueilBesoinController@validerforbesoinannulation');

Route::post('/annulationbesoinjournalier/{idbesoinjournalier}', 'BesoinJournalierController@updateforbesoinannulation');

Route::post('/miseajourrecep/{param}', 'ReceptionController@validerreceptionpartiellefinale');

Route::group(['prefix' => 'chantiers'], function () {
    Route::apiResource('/{chantier}/commandes', 'ChantierCommandesController');
    Route::apiResource('/{chantier}/receptions', 'ChantierReceptionsController');
    Route::apiResource('/{chantier}/inventaires', 'ChantierStockController');
    Route::apiResource('/{chantier}/inventairesprefat', 'ChantierStockPrefatController');
    Route::apiResource('/{chantier}/sections', 'SectionController');
    Route::apiResource('/{chantier}/prefats', 'PrefatController');
    Route::apiResource('/{chantier}/articles', 'ChantierArticleMouvementStock');
    Route::apiResource('/{chantier}/recueilbesoins', 'RecueilBesoinController');
    Route::apiResource('/{chantier}/besoinjournaliers', 'BesoinJournalierController');
    Route::apiResource('/{chantier}/enlevements', 'EnlevementController');
});



Route::apiResource('/employes', 'EmployeController');
Route::apiResource('/societeconstructives', 'SocieteconstructiveController');
Route::apiResource('/typemaisons', 'TypemaisonController');
Route::apiResource('/maisons', 'MaisonController');
Route::apiResource('/etapesconstructions', 'EtapeconstructionController');
// Route::apiResource('/recueilbesoins', 'RecueilBesoinController');
Route::apiResource('/receptions', 'ReceptionController');
Route::apiResource('/unitemesures', 'UniteMesureController');
Route::apiResource('/articles', 'ArticleController');
Route::apiResource('/typechantiers', 'TypeChantierController');
Route::apiResource('/fournisseurs', 'FournisseurController');
Route::apiResource('/commandes', 'CommandeController');
Route::apiResource('/recueils', 'RecueilBesoinController');
Route::apiResource('/besoins', 'BesoinJournalierController');
Route::group(['prefix' => 'commandes'], function () {
    Route::apiResource('/{commande}/receptions', 'CommandeReceptionController');
    Route::apiResource('/{commande}/detailscommandes', 'CommandeItemController');
});

Route::apiResource('/stocks', 'StockController');
Route::apiResource('/stockemplacements', 'StockEmplacementController');

Route::apiResource('/permissions', 'PermissionController');
Route::apiResource('/roles', 'RoleController');
