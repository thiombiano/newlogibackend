<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnConstraintLivreurToTableEnlevements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enlevements', function (Blueprint $table) {
            $table->bigInteger('livreur_id')->unsigned()->nullable();
            $table->foreign('livreur_id')->references('id')->on('livreurs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enlevements', function (Blueprint $table) {
            $table->dropForeign('enlevements_livreur_id_foreign');
            $table->dropColumn('livreur_id');
        });
    }
}
