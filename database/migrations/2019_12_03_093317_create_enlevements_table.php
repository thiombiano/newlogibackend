<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnlevementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enlevements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_En')->nullable();
            $table->dateTime('date_En')->nullable();
            $table->boolean('statut_En')->nullable();
            $table->text('commentaires')->nullable();
            $table->bigInteger('chantier_id')->unsigned()->nullable();
            $table->foreign('chantier_id')->references('id')->on('chantiers')->onDelete('cascade');
            $table->bigInteger('magasinier_id')->unsigned()->nullable();
            $table->foreign('magasinier_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enlevements');
    }
}
