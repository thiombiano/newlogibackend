<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsBicAndValueofbicAndIsTvaToCommandeitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commande_items', function (Blueprint $table) {
            $table->boolean('is_bic')->default(false);
            $table->float('valueofbic')->nullable();
            $table->boolean('is_tva')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commande_items', function (Blueprint $table) {
            $table->dropColumn('is_bic');
            $table->dropColumn('valueofbic');
            $table->dropColumn('is_tva');
        });
    }
}
