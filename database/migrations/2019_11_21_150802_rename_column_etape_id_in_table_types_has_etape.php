<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnEtapeIdInTableTypesHasEtape extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_has_etapes', function (Blueprint $table) {
            $table->renameColumn('etape_id', 'etape_construction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_has_etapes', function (Blueprint $table) {
            $table->renameColumn('etape_construction_id ', 'etape_id');
        });
    }
}
