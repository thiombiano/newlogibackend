<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBesoinJitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('besoin_jitems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('besoin_journalier_id')->unsigned()->nullable();
            $table->foreign('besoin_journalier_id')->references('id')->on('besoin_journaliers')->onDelete('cascade');
            $table->bigInteger('maison_id')->unsigned()->nullable();
            $table->foreign('maison_id')->references('id')->on('maisons')->onDelete('cascade');
            $table->bigInteger('etape_construction_id')->unsigned()->nullable();
            $table->foreign('etape_construction_id')->references('id')->on('etape_constructions')->onDelete('cascade');
            $table->bigInteger('article_id')->unsigned()->nullable();
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->integer('quantiteBesoin');
            $table->string('commentaires')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('besoin_jitems');
    }
}
