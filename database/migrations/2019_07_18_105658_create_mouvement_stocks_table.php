<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMouvementStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mouvement_stocks', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('stock_id');
            $table->foreign('stock_id')->references('id')->on('stocks')->onDelete('cascade');

            $table->unsignedBigInteger('operation_id');
            $table->string('operation_type'); //'livraison', 'demande', 'restitution'
            $table->string('operation_sens'); //entree/sortie
            $table->integer('quantiteAvant');
            $table->integer('quantite');
            $table->integer('quantiteApres');
            $table->dateTime('date_RC');
            $table->string('commentaires')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mouvement_stocks');
    }
}
