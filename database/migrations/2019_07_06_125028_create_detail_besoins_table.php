<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailBesoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_besoins', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('recueil_besoin_id');
            $table->foreign('recueil_besoin_id')->references('id')->on('recueil_besoins')->onDelete('cascade');

            $table->unsignedBigInteger('article_id');
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');

            $table->integer('quantiteDemandee');

            $table->integer('maison_id')->nullable();
           // $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');

            $table->string('commentaires')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_besoins');
    }
}
