<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceptionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reception_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('reception_id');
            $table->foreign('reception_id')->references('id')->on('receptions')->onDelete('cascade');

            $table->dateTime('date_RC');

            $table->unsignedBigInteger('article_id');
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');

            $table->unsignedBigInteger('commande_item_id');
            $table->foreign('commande_item_id')->references('id')->on('commande_items')->onDelete('cascade');

            $table->integer('quantite_receptionnee');

            $table->string('commentaires')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reception_items');
    }
}
