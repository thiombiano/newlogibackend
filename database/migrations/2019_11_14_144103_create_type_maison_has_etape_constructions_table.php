<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeMaisonHasEtapeConstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_has_etapes', function (Blueprint $table) {
            $table->bigInteger('type_maison_id')->unsigned()->nullable();
            $table->bigInteger('etape_id')->unsigned()->nullable();
            $table->foreign('type_maison_id')->references('id')->on('type_maisons')->onDelete('cascade');
            $table->foreign('etape_id')->references('id')->on('etape_constructions')->onDelete('cascade');
            $table->primary(['type_maison_id', 'etape_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_maison_has_etape_constructions');
    }
}
