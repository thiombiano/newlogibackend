<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('libelle_article');
            $table->string('reference')->nullable();
            $table->string('description')->nullable();
            $table->integer('prix_referent')->nullable();

            $table->unsignedBigInteger('unite_mesure_id')->nullable();
            $table->foreign('unite_mesure_id')->references('id')->on('unite_mesures')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
