<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCodeemployeTelemployePrenom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('codeemploye')->nullable();
            $table->string('telemploye')->nullable();
            $table->string('prenomemploye')->nullable();
            $table->boolean('isActif')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('codeemploye');
            $table->dropColumn('telemploye');
            $table->dropColumn('prenomemploye');
            $table->dropColumn('isActif');
        });
    }
}
