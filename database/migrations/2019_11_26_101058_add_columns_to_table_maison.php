<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTableMaison extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('maisons', function (Blueprint $table) {
            $table->string('superficie')->nullable();
            $table->string('client')->nullable();
            $table->string('codevirtuel')->nullable();
            $table->unsignedBigInteger('controleur_id')->nullable();
            $table->foreign('controleur_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('maisons', function (Blueprint $table) {
            $table->dropColumn('superficie');
            $table->dropColumn('client');
            $table->dropColumn('codevirtuel');
            $table->dropColumn('controleur_id');
            $table->dropForeign('maisons_controleur_id_foreign');
        });
    }
}
