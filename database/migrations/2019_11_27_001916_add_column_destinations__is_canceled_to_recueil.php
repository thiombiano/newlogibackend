<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDestinationsIsCanceledToRecueil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recueil_besoins', function (Blueprint $table) {
            $table->boolean('isCanceled')->default(false);
            $table->text('canceledcommentaire')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recueil_besoins', function (Blueprint $table) {
            $table->dropColumn('isCanceled');
            $table->dropColumn('canceledcommentaire');
        });
    }
}
