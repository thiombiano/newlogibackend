<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageetapesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imageetapes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('etapeencour_id')->unsigned()->nullable();
            $table->string('filename')->nullable();
            $table->foreign('etapeencour_id')->references('id')->on('etape_state_in_maisons')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imageetapes');
    }
}
