<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumeTypeEnlevementToTableEnlevements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enlevements', function (Blueprint $table) {
            $table->string('typeenlevement')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enlevements', function (Blueprint $table) {
            $table->dropColumn('typeenlevement');
        });
    }
}
