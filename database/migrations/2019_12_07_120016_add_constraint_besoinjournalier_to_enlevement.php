<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintBesoinjournalierToEnlevement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enlevements', function (Blueprint $table) {
            $table->bigInteger('besoin_journalier_id')->unsigned()->nullable();
            $table->foreign('besoin_journalier_id')->references('id')->on('besoin_journaliers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enlevements', function (Blueprint $table) {
            $table->dropColumn('besoin_journalier_id');
            $table->dropForeign('besoin_journaliers_besoin_journalier_id_foreign');
        });
    }
}
