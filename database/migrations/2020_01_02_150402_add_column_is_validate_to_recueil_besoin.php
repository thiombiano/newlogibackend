<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsValidateToRecueilBesoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recueil_besoins', function (Blueprint $table) {
            $table->boolean('isValidate')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recueil_besoins', function (Blueprint $table) {
            $table->dropColumn('isValidate');
        });
    }
}
