<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintFonctionIdToTableBesoinJitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('besoin_jitems', function (Blueprint $table) {
            $table->bigInteger('fonction_id')->unsigned()->nullable();
            $table->foreign('fonction_id')->references('id')->on('fonctions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('besoin_jitems', function (Blueprint $table) {
            $table->dropColumn('fonction_id');
            $table->dropForeign('besoin_jitems_fonction_id_foreign');
        });
    }
}
