<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTableJournees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('journees', function (Blueprint $table) {
            $table->dateTime('dateouverturejournee')->nullable();
            $table->dateTime('datefermeturejournee')->nullable();
            $table->integer('totalbesoinjournalier');
            $table->boolean('etatjournee')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('journees', function (Blueprint $table) {
            $table->dropColumn('dateouverture');
            $table->dropColumn('datefermeture');
            $table->dropColumn('etatjournee');
            $table->dropColumn('totalbesoinjournalier');
        });
    }
}
