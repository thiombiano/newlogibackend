<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeySocieteConstructiveToTableEmployes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employes', function (Blueprint $table) {
            $table->bigInteger('societe_constructive_id')->unsigned()->nullable();
            $table->foreign('societe_constructive_id')->references('id')->on('societe_constructives')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employes', function (Blueprint $table) {
            $table->dropColumn('societe_constructive_id');
            $table->dropForeign('employes_societe_constructive_id_foreign');
        });
    }
}
