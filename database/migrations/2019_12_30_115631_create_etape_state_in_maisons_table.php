<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtapeStateInMaisonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etape_state_in_maisons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('maison_id')->unsigned()->nullable();
            $table->bigInteger('etape_construction_id')->unsigned()->nullable();
            $table->dateTime('dateouvertureetape')->nullable();
            $table->dateTime('datefermetureetape')->nullable();
            $table->boolean('etapeState')->default(true);
            $table->boolean('isFinished')->default(false);
            $table->text('commentaires')->nullable();
            $table->integer('nombretotaletape')->nullable();
            $table->integer('numeroetapencour')->nullable();
            $table->foreign('maison_id')->references('id')->on('maisons')->onDelete('cascade');
            $table->foreign('etape_construction_id')->references('id')->on('etape_constructions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etape_state_in_maisons');
    }
}
