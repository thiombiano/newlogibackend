<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsMontantsToTableEtapes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('etape_constructions', function (Blueprint $table) {
            $table->float('montant_brut')->nullable();
            $table->float('montant_macon')->nullable();
            $table->float('montant_verse_entreprise')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('etape_constructions', function (Blueprint $table) {
            $table->dropColumn('montant_brut');
            $table->dropColumn('montant_macon');
            $table->dropColumn('montant_verse_entreprise');
        });
    }
}
