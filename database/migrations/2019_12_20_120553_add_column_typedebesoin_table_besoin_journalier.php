<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypedebesoinTableBesoinJournalier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('besoin_journaliers', function (Blueprint $table) {
            $table->string('typebesoin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('besoin_journaliers', function (Blueprint $table) {
            $table->dropColumn('typebesoin');
        });
    }
}
