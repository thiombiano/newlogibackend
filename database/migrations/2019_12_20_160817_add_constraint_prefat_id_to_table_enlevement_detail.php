<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintPrefatIdToTableEnlevementDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enlevement_items', function (Blueprint $table) {
            $table->bigInteger('prefat_id')->unsigned()->nullable();
            $table->foreign('prefat_id')->references('id')->on('prefats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enlevement_items', function (Blueprint $table) {
            $table->dropForeign('enlevement_items_prefat_id_foreign');
            $table->dropColumn('prefat_id');
        });
    }
}
