<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prestataire_id')->unsigned()->nullable();
            $table->bigInteger('maison_id')->unsigned()->nullable();
            $table->bigInteger('etape_construction_id')->unsigned()->nullable();
            $table->dateTime('datedebut')->nullable();
            $table->dateTime('datefin')->nullable();
            $table->string('typecontrat')->nullable();
            $table->foreign('prestataire_id')->references('id')->on('employes')->onDelete('cascade');
            $table->foreign('maison_id')->references('id')->on('maisons')->onDelete('cascade');
            $table->foreign('etape_construction_id')->references('id')->on('etape_constructions')->onDelete('cascade');
            $table->boolean('isCanceled')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestations');
    }
}
