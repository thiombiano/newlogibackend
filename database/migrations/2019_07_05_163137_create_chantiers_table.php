<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChantiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chantiers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code_chantier');
            $table->string('denomination')->nullable();
            $table->string('description')->nullable();
            $table->string('adresse')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->string('url_photo')->nullable();
            $table->timestamps();


            $table->unsignedBigInteger('chef_chantier_id');
            $table->foreign('chef_chantier_id')->references('id')->on('employes')->onDelete('cascade');

            $table->unsignedBigInteger('type_chantier_id');
            $table->foreign('type_chantier_id')->references('id')->on('type_chantiers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chantiers');
    }
}
