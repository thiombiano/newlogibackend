<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMouvementStockEmplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mouvement_stock_emplacements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stock_emp_id')->nullable();
            $table->foreign('stock_emp_id')->references('id')->on('stock_emplacements')->onDelete('cascade');
            $table->unsignedBigInteger('operation_id')->nullable();
            $table->string('operation_type')->nullable(); //'livraison', 'demande', 'restitution'
            $table->string('operation_sens')->nullable(); //entree/sortie
            $table->integer('quantiteAvant');
            $table->integer('quantite');
            $table->integer('quantiteApres');
            $table->dateTime('date_Em');
            $table->string('commentaires')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mouvement_stock_emplacements');
    }
}
