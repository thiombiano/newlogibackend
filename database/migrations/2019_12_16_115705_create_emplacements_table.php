<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emplacements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomEmplacement')->nullable();
            $table->string('descriptionEmplacement')->nullable();
            $table->string('adresseEmplacement')->nullable();
            $table->bigInteger('chantier_id')->unsigned()->nullable();
            $table->foreign('chantier_id')->references('id')->on('chantiers')->onDelete('cascade');
            $table->string('commentaires')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emplacements');
    }
}
