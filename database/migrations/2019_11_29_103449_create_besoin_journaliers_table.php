<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBesoinJournaliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('besoin_journaliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('journee_id')->unsigned()->nullable();
            $table->foreign('journee_id')->references('id')->on('journees')->onDelete('cascade');
            $table->bigInteger('chantier_id')->unsigned()->nullable();
            $table->foreign('chantier_id')->references('id')->on('chantiers')->onDelete('cascade');
            $table->bigInteger('technicien_id')->unsigned()->nullable();
            $table->foreign('technicien_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('besoin_journaliers');
    }
}
