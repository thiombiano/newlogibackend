<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterConstraintOnTableChantiers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chantiers', function (Blueprint $table) {
            $table->dropForeign('chantiers_chef_chantier_id_foreign');
            $table->dropColumn('chef_chantier_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chantiers', function (Blueprint $table) {
            $table->unsignedBigInteger('chef_chantier_id');
            $table->foreign('chef_chantier_id')->references('id')->on('employes')->onDelete('cascade');
        });
    }
}
