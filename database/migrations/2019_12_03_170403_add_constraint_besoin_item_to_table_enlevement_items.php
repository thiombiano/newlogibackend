<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintBesoinItemToTableEnlevementItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enlevement_items', function (Blueprint $table) {
            $table->unsignedBigInteger('besoin_jitems_id')->nullable();
            $table->foreign('besoin_jitems_id')->references('id')->on('besoin_jitems')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enlevement_items', function (Blueprint $table) {
            $table->dropForeign('enlevement_items_besoin_jitems_id_foreign');
            $table->dropColumn('besoin_jitems_id');
        });
    }
}
