<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrefatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prefats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('libelleprefat');
            $table->boolean('is_actif')->default(true);
            $table->unsignedBigInteger('chantier_id')->nullable();
            $table->foreign('chantier_id')->references('id')->on('chantiers')->onDelete('cascade');
            $table->unsignedBigInteger('technicien_id')->nullable();
            $table->foreign('technicien_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('magasinier_id')->unsigned()->nullable();
            $table->foreign('magasinier_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prefats');
    }
}
