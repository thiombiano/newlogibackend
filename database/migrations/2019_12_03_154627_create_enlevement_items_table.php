<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnlevementItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enlevement_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date_En')->nullable();
            $table->integer('quantiteASortir')->nullable();
            $table->unsignedBigInteger('article_id');
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->bigInteger('maison_id')->unsigned()->nullable();
            $table->foreign('maison_id')->references('id')->on('maisons')->onDelete('cascade');
            $table->bigInteger('etape_construction_id')->unsigned()->nullable();
            $table->foreign('etape_construction_id')->references('id')->on('etape_constructions')->onDelete('cascade');
            $table->bigInteger('enlevement_id')->unsigned()->nullable();
            $table->foreign('enlevement_id')->references('id')->on('enlevements')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enlevement_items');
    }
}
