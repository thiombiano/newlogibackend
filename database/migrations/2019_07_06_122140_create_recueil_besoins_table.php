<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecueilBesoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recueil_besoins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numeroRB');
            $table->dateTime('dateRB');
            $table->unsignedBigInteger('statutRB');
            $table->dateTime('dateLivraisonCible');
            $table->unsignedBigInteger('chantier_id');
            $table->foreign('chantier_id')->references('id')->on('chantiers')->onDelete('cascade');

            $table->boolean('isVisaDemandeur')->default(false);
            $table->boolean('isVisaConducteurChantier')->default(false);
            $table->boolean('isVisaResponsableStock')->default(false);
            $table->boolean('isVisaDirecteurTechnique')->default(false);
            $table->boolean('isVisaResponsableAchat')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recueil_besoins');
    }
}
