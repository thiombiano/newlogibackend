<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Exécution d'une factorie
        // factory(App\Models\Article::class, 20)->create();
        //Ajout de fonctions
        DB::table('fonctions')->insert([
            'codeFonction' => 'CDC',
            'libelleFonction' => 'Chef de Chantier'
        ]);

        DB::table('fonctions')->insert([
            'codeFonction' => 'MGN',
            'libelleFonction' => 'Magasinier'
        ]);

        DB::table('fonctions')->insert([
            'codeFonction' => 'TEC',
            'libelleFonction' => 'Technicien'
        ]);

        DB::table('fonctions')->insert([
            'codeFonction' => 'CTL',
            'libelleFonction' => 'Contôleur'
        ]);

        //Ajout d'employes
        DB::table('employes')->insert([
            'codeEmploye' => '0001',
            'prenom' => 'Georges',
            'nom' => 'Kaboré',
            'tel' => '78 65 43 53',
            'email' => 'georges.kabore@sodigaz.com',
            'fonction_id' => 1
        ]);
        DB::table('employes')->insert([
            'codeEmploye' => '0002',
            'prenom' => 'Mamadou',
            'nom' => 'Ouédraogo',
            'tel' => '65 43 25 76',
            'email' => 'mamadou.ouedraogo@sodigaz.com',
            'fonction_id' => 2
        ]);
        DB::table('employes')->insert([
            'codeEmploye' => '0003',
            'prenom' => 'Ousmane',
            'nom' => 'Tall',
            'tel' => 'Z5 65 74 89',
            'email' => 'ousmane.tall@sodigaz.com',
            'fonction_id' => 3
        ]);

        DB::table('employes')->insert([
            'codeEmploye' => '0004',
            'prenom' => 'Lucien',
            'nom' => 'Zongo',
            'tel' => '76 54 78 34',
            'email' => 'lucien.zongo@sodigaz.com',
            'fonction_id' => 2
        ]);
        DB::table('employes')->insert([
            'codeEmploye' => '0005',
            'prenom' => 'Abdou Karim',
            'nom' => 'Sanogo',
            'tel' => '43 67 65 27',
            'email' => 'karim.sanogo@sodigaz.com',
            'fonction_id' => 4
        ]);
        DB::table('employes')->insert([
            'codeEmploye' => '0006',
            'prenom' => 'Abdoulaye',
            'nom' => 'Touré',
            'tel' => '64 87 65 34',
            'email' => 'abdoulaye.toure@sodigaz.com',
            'fonction_id' => 1
        ]);
        DB::table('employes')->insert([
            'codeEmploye' => '0007',
            'prenom' => 'Martin',
            'nom' => 'Zamoura',
            'tel' => '64 78 09 63',
            'email' => 'martin.zamoura@sodigaz.com',
            'fonction_id' => 1
        ]);
        //Ajout de typeChantier
        DB::table('type_chantiers')->insert([
            'code_type_chantier' => 'LGC',
            'libelle_type_chantier' => 'Logic'
        ]);
        DB::table('type_chantiers')->insert([
            'code_type_chantier' => 'SDG',
            'libelle_type_chantier' => 'Sodigaz'
        ]);
        DB::table('type_chantiers')->insert([
            'code_type_chantier' => 'ETA',
            'libelle_type_chantier' => 'Etat'
        ]);
        DB::table('type_chantiers')->insert([
            'code_type_chantier' => 'PRV',
            'libelle_type_chantier' => 'Privé'
        ]);

        //Ajout de chantiers
        DB::table('chantiers')->insert([
            'code_chantier' => 'BSK1',
            'denomination' => 'Bassinko 1',
            'description' => 'Projet de contruction de 1000 logements économiques',
            'adresse' => 'Bassinko',
            'latitude' => '12.391926',
            'longitude' => '-1.635606',
            'chef_chantier_id' => 1,
            'type_chantier_id' => 1,
        ]);
        DB::table('chantiers')->insert([
            'code_chantier' => 'BSK2',
            'denomination' => 'Bassinko 2',
            'description' => 'Projet de contruction de 700 logements',
            'adresse' => 'Bassinko',
            'latitude' => '12.391926',
            'longitude' => '-1.635606',
            'chef_chantier_id' => 6,
            'type_chantier_id' => 1,
        ]);
        DB::table('chantiers')->insert([
            'code_chantier' => 'BSK3',
            'denomination' => 'Bassinko 3',
            'description' => 'Projet de contruction de 2500 logements économiques en collaboration avec l\'Etat',
            'adresse' => 'Bassinko',
            'latitude' => '12.391926',
            'longitude' => '-1.635606',
            'chef_chantier_id' => 7,
            'type_chantier_id' => 2,
        ]);
        // Ajout des Unités de mesure
        DB::table('unite_mesures')->insert([
            'code' => 'unité',
            'libelle' => 'Unité'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'm3',
            'libelle' => 'Metre Cube'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'sac',
            'libelle' => 'Sac'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'rouleau',
            'libelle' => 'Rouleau'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'kg',
            'libelle' => 'Kilogramme'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'boite',
            'libelle' => 'Boite'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'm',
            'libelle' => 'Mètre'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'l',
            'libelle' => 'Litre'
        ]);
        DB::table('unite_mesures')->insert([
            'code' => 'paquet',
            'libelle' => 'Paquet'
        ]);
        //Ajout d'articles
        DB::table('articles')->insert([
            'libelle_article' => 'Tube PVC de 100',
            'reference' => 'TPVC100',
            'description' => 'Tube utilisée pour l\'évacuation des eaux usées TUBE EVACUATION NFE ME Diam.100 mm L.4 m
            Diamètre: 100mm
            Longueur: 2 mètres',
            'prix_referent' => 8500,
            'unite_mesure_id' => 1
        ]);

        DB::table('articles')->insert([
            'libelle_article' => 'Coude PPR de 20',
            'reference' => 'CPPR20',
            'description' => 'Raccord coudé 20/45 ensemble de 10 pièces, Té, manchon, réduction',
            'prix_referent' => 700,
            'unite_mesure_id' => 1
        ]);


        // //Ajout de receuils de besoins d'un chantier
        // DB::table('recueil_besoins')->insert([
        //     'numeroRB' => 'BAS1RB0001',
        //     'dateRB' => \Carbon\Carbon::now(),
        //     'statutRB' => 1,
        //     'chantier_id' => 1,
        //     'dateLivraisonCible' => \Carbon\Carbon::now()->addDays(7),
        // ]);
        // DB::table('recueil_besoins')->insert([
        //     'numeroRB' => 'BAS1RB0002',
        //     'dateRB' => \Carbon\Carbon::now()->addDays(5),
        //     'statutRB' => 1,
        //     'chantier_id' => 1,
        //     'dateLivraisonCible' => \Carbon\Carbon::now()->addDays(7),
        // ]);

        // //Ajout de détails besoin d'un recueil de besoins
        // DB::table('detail_besoins')->insert([
        //     'recueil_besoin_id' => 1,
        //     'article_id' => 1,
        //     'quantiteDemandee' => 20,
        //     'maison_id' => 1,
        //     'commentaires' => 'De préférable de couleur grise ',
        // ]);

        // DB::table('detail_besoins')->insert([
        //     'recueil_besoin_id' => 1,
        //     'article_id' => 2,
        //     'quantiteDemandee' => 50,
        //     'maison_id' => 1,
        //     'commentaires' => '',
        // ]);

        //Ajout de fournisseurs
        DB::table('fournisseurs')->insert([
            'code_fournisseur' => 'FRN001',
            'raison_social' => 'Batimat',
            'adresse' => 'N° 38474, rue 25101, Ouaga 2000',
            'tel' => '67 65 48 93',
            'email' => 'batimat@orange.com',
            'rc' => 'BF OUA 2019 M 9876',
            'ninea' => '',
        ]);
        DB::table('fournisseurs')->insert([
            'code_fournisseur' => 'FRN002',
            'raison_social' => 'ECAF',
            'adresse' => 'N° 8729873, rue 25101, Patte d\'Oie',
            'tel' => '85 65 48 58',
            'email' => 'ecafbf@orange.com',
            'rc' => 'BF OUA 2016 M 8797',
            'ninea' => '',
        ]);
        //Ajouts des paramètres de gestion des numéros automatiques
        DB::table('parametres')->insert([
            'code_param' => 'BCBSK1',
            'nom_param' => 'Numérotation Bon de commande Bassinko 1',
            'string_val' => '',
            'num_val' => 1,
        ]);
        DB::table('parametres')->insert([
            'code_param' => 'BRBSK1',
            'nom_param' => 'Numérotation Bon de réception Bassinko 1',
            'string_val' => '',
            'num_val' => 1,
        ]);
        //Ajout de receuils de besoins d'un chantier
        DB::table('commandes')->insert([
            'numero_BC' => 'BC009872',
            'numero_DA' => 'DA007546',
            'date_BC' => \Carbon\Carbon::now(),
            'statut_BC' => 1,
            'chantier_id' => 1,
            'fournisseur_id' => 1,
            'montant_ttc' => 1500000,
            'date_lvraison_prevue' => \Carbon\Carbon::now()->addDays(7),
            'is_prix_ttc' => false

        ]);
        DB::table('commandes')->insert([
            'numero_BC' => 'BC009873',
            'numero_DA' => 'DA008765',
            'date_BC' => \Carbon\Carbon::now()->addDays(-27),
            'statut_BC' => 1,
            'chantier_id' => 1,
            'fournisseur_id' => 2,
            'montant_ttc' => 1200000,
            'date_lvraison_prevue' => \Carbon\Carbon::now()->addDays(-20),
            'is_prix_ttc' => false
        ]);

        DB::table('commande_items')->insert([
            'commande_id' => 1,
            'article_id' => 1,
            'quantite_commandee' => 50,
            'prix_unitaire' => 1400,
            'commentaires' => ""
        ]);
        DB::table('commande_items')->insert([
            'commande_id' => 1,
            'article_id' => 2,
            'quantite_commandee' => 100,
            'prix_unitaire' => 750,
            'commentaires' => ""
        ]);

        //Ajout de reception livraison fournisseur
        DB::table('receptions')->insert([
            'numero_RC' => 'BAS1RC0001',
            'date_RC' => \Carbon\Carbon::now(),
            'statut_RC' => 1,
            'numero_piece_fournisseur' => 'BL98397242',
            'commande_id' => 1,
            'fournisseur_id' => 2,
            'chantier_id' => 1,
            'has_visa_responsable_ressource' => true,
            'has_visa_conducteur_chantier' => true,
            'has_visa_magasinier' => true,
        ]);

        DB::table('receptions')->insert([
            'numero_RC' => 'BAS1RC0002',
            'date_RC' => \Carbon\Carbon::now()->addDays(5),
            'statut_RC' => 1,
            'numero_piece_fournisseur' => 'REF9873843',
            'commande_id' => 2,
            'fournisseur_id' => 2,
            'chantier_id' => 1,
            'has_visa_responsable_ressource' => true,
            'has_visa_conducteur_chantier' => true,
            'has_visa_magasinier' => true,
        ]);

        //Ajout de détails besoin d'un recueil de besoins
        DB::table('reception_items')->insert([
            'reception_id' => 1,
            'article_id' => 1,
            'commande_item_id' => 1,
            'quantite_receptionnee' => 25,
            'commentaires' => '',
            'date_RC' => \Carbon\Carbon::now(),
        ]);

        DB::table('reception_items')->insert([
            'reception_id' => 1,
            'article_id' => 2,
            'commande_item_id' => 2,
            'quantite_receptionnee' => 70,
            'commentaires' => '',
            'date_RC' => \Carbon\Carbon::now(),
        ]);

        //Ajout de dépots
        DB::table('depots')->insert([
            'nomDepot' => 'Depot1',
            'descriptionDepot' => 'Dépôt du matériel de plomberie',
            'adresseDepot' => 'Container Bleu à droite de l\'entrée principale du chantier',
            'chantier_id' => 1,
            'commentaires' => '',
        ]);

        DB::table('depots')->insert([
            'nomDepot' => 'Depot2',
            'descriptionDepot' => 'Dépôt du matériel de maçonnerie et des matériaux de constrution',
            'adresseDepot' => 'Hangar au centre du chantier',
            'chantier_id' => 2,
            'commentaires' => '',
        ]);

        //Ajout de stocks
        DB::table('stocks')->insert([
            'depot_id' => 1,
            'article_id' => 1,
            'stockMinimal' => 10,
            'quantiteDisponible' => 25,
            'commentaires' => '',
        ]);

        DB::table('stocks')->insert([
            'depot_id' => 1,
            'article_id' => 2,
            'stockMinimal' => 30,
            'quantiteDisponible' => 70,
            'commentaires' => '',
        ]);

        //Ajout de stocks
        DB::table('mouvement_stocks')->insert([
            'stock_id' => 1,
            'operation_id' => 1,
            'operation_type' => 'Reception',
            'operation_sens' => 'Entrée',
            'quantiteAvant' => 0,
            'quantite' => 25,
            'quantiteApres' => 25,
            'commentaires' => '',
            'date_RC' => \Carbon\Carbon::now(),
        ]);
        DB::table('mouvement_stocks')->insert([
            'stock_id' => 2,
            'operation_id' => 2,
            'operation_type' => 'Reception',
            'operation_sens' => 'Entrée',
            'quantiteAvant' => 0,
            'quantite' => 70,
            'quantiteApres' => 70,
            'commentaires' => '',
            'date_RC' => \Carbon\Carbon::now(),
        ]);
    }
}
