<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Chantier;
use Faker\Generator as Faker;

$factory->define(Chantier::class, function (Faker $faker) {
    return [
        'codeChantier' => $faker->word,
        'denomination' => $faker->sentence,
        'description' => $faker->paragraph,
        'adresse' => $faker->address,
    ];
});
