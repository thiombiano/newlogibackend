<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BesoinJitem extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['besoin_journalier_id', 'maison_id', 'fonction_id', 'is_out', 'etape_construction_id', 'article_id', 'quantiteBesoin', 'commentaires', 'prefat_id'];

    public function enlevement_items()
    {
        return $this->belongsTo(EnlevementItem::class);
    }

}
