<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Journee extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['dateouverturejournee', 'datefermeturejournee', 'etatjournee', 'totalbesoinjournalier'];
}
