<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class TypeChantier extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['code_type_chantier', 'libelle_type_chantier'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    public function chantiers()
    {
        return $this->hasMany(Chantier::class);
    }
}
