<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fournisseur extends Model
{
    protected $hidden = ['created_at','updated_at'];

    protected $fillable = [
        'code_fournisseur',
        'raison_social',
        'adresse',
        'tel',
        'email',
        'rc',
        'ninea',
        'commentforcanceled',
        'is_canceled'
    ];

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }

    public function receptions()
    {
        return $this->hasMany(Reception::class);
    }
}
