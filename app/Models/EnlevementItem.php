<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnlevementItem extends Model
{
    protected $fillable = ['date_En', 'quantiteASortir', 'article_id', 'maison_id', 'fonction_id', 'etape_construction_id', 'enlevement_id'];

    public function besoin_jitems()
    {
        return $this->hasMany(BesoinJitem::class);
    }

    public function enlevement()
    {
        return $this->belongsTo(Enlevement::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
