<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EtapeStateInMaison extends Model
{
    protected $fillable = ['maison_id', 'etape_construction_id', 'dateouvertureetape', 'datefermetureetape', 'etapeState', 'isFinished', 'commentaires', 'nombretotaletape', 'numeroetapencour'];
}
