<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imageetape extends Model
{
    protected $fillable = ['etapeencour_id', 'filename'];
}
