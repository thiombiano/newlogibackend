<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EtapeConstruction extends Model
{
    protected $fillable = ['libelleetape', 'description'];
    protected $hidden = ['created_at', 'updated_at'];

    public function type_maisons(){
        return $this->belongsToMany(TypeMaison::class);
    }

    public function prestataires(){
        return $this->belongsToMany(Prestation::class,'prestations');
    }

    public function maisons(){
        return $this->belongsToMany(Maison::class,'prestations');
    }

}
