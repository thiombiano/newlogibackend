<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MouvementStock extends Model
{
    protected $fillable = [
        'stock_id',
        'operation_id',
        'operation_type',
        'operation_sens',
        'quantite',
        'commentaires'
    ];

    public function stock()
    {
        return $this->belongsTo(Stock::class);
    }
}
