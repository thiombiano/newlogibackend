<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reception extends Model
{
    protected $fillable = [
        'numero_RC',
        'date_RC',
        'statut_RC',
        'numero_piece_fournisseur',
        'chantier_id',
        'commande_id',
        'fournisseur_id',
        'has_visa_responsable_ressource',
        'has_visa_conducteur_chantier',
        'has_visa_magasinier',
        'livreur',
        'telephone_livreur',
        'immatriculation',
        'canceledcommentaire',
        'motifpasimmatriculation',
        'isCanceled',
        'typedereception'

    ];
    public function chantier()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function fournisseur()
    {
        return $this->belongsTo(Fournisseur::class);
    }

    public function commande()
    {
        return $this->belongsTo(Commande::class);
    }

    public function reception_items()
    {
        return $this->hasMany(ReceptionItem::class);
    }


    // public function articles()
    // {
    //     return $this->belongsToMany(Article::class, 'reception_articles')->withPivot(['id', 'article', 'quantite', 'commande_id', 'commentaires']);
    // }
}
