<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Article extends Model  implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $fillable = [
        'libelle_article',
        'reference',
        'description',
        'prix_referent',
        'is_tva',
        'is_bic',
        'valueofbic'
    ];

    public function recueilBesoins()
    {
        return $this->belongsToMany(RecueilBesoin::class, 'detail_besoins')->withPivot(['quantiteDemandee', 'maison_id', 'commentaires']);
    }

    public function receptions()
    {
        return $this->belongsToMany(Reception::class, 'reception_articles')->withPivot(['id', 'article', 'quantite', 'commande_id', 'commentaires']);
    }

    public function commandes()
    {
        return $this->belongsToMany(Commande::class, 'commande_articles')->withPivot(['quantite', 'prixUnitaire', 'commentaires']);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function unite_mesure()
    {
        return $this->belongsTo(UniteMesure::class);
    }
}
