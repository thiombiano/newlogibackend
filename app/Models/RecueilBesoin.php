<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Chantier;

class RecueilBesoin extends Model
{
    protected $fillable = [
        'numeroRB',
        'dateRB',
        'statutRB',
        'chantier_id',
        'dateLivraisonCible',
        'destinations',
        'isCanceled',
        'canceledcommentaire'
    ];

    public function chantier()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'detail_besoins')->withPivot(['quantiteDemandee', 'maison_id', 'commentaires']);
    }


}
