<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersHasRoles extends Model
{
    public $timestamps = false;
    protected $fillable = ['role_id', 'user_id'];
}
