<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailBesoin extends Model
{
    protected $fillable = [
        'id',
        'recueil_besoin_id',
        'article_id',
        'quantiteDemandee'
    ];
    public function detailReceptions()
    {
        return $this->hasMany(DetailReception::class);
    }
}
