<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocieteConstructive extends Model
{
    protected $fillable = ['libellesociete', 'description'];
    protected $hidden = ['created_at', 'updated_at'];

    public function maisons(){
        return $this->hasMany(Maison::class);
    }
}
