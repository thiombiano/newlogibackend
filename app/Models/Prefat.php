<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Prefat extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['libelleprefat', 'is_actif', 'chantier_id', 'technicien_id', 'magasinier_id'];


    public function chantiers()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function technicien(){
        return $this->belongsTo(User::class);
    }

    public function magasinier(){
        return $this->belongsTo(User::class);
    }
}
