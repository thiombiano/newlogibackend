<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Livreur extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['nomlivreur', 'prenomlivreur', 'telephonelivreur', 'cniblivreur'];
}
