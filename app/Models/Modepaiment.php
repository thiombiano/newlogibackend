<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modepaiment extends Model
{
    protected $hidden = ['created_at','updated_at'];

    protected $fillable = ['id','libellemodepaiement','modepaiment_id'];

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }
}
