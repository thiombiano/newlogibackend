<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['libellerole', 'descriptionrole'];

    public function permissions(){
        return $this->belongsToMany(Permission::class,'roles_has_permissions');
    }

}

