<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Depot extends Model
{
    protected $fillable = ['nomDepot', 'descriptionDepot', 'adresseDepot', 'chantier_id', 'commentaires'];
    public function chantier()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }
}
