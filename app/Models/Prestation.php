<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prestation extends Model
{
    public $timestamps = false;
    protected $fillable = ['prestataire_id', 'maison_id', 'etape_construction_id', 'datedebut', 'datefin', 'typecontrat', 'date_Prestation'];
}
