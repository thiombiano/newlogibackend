<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['libellesection', 'is_actif'];
    protected $hidden = ['created_at', 'updated_at'];

    public function chantiers()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function lots()
    {
        return $this->hasMany(Lot::class);
    }
}
