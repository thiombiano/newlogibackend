<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolesHasPermission extends Model
{
    public $timestamps = false;
    protected $fillable = ['role_id', 'permission_id'];
}
