<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Commande;

class CommandeItem extends Model
{
    protected $fillable = [
        'id',
        'commande_id',
        'article_id',
        'quantite_commandee',
        'prix_unitaire',
        'commentaires',
        'is_tva',
        'is_bic',
        'valueofbic'
    ];

    public function commande()
    {
        return $this->belongsTo(Commande::class);
    }
    public function article()
    {
        return $this->belongsTo(Article::class);
    }
    public function reception_items()
    {
        return $this->hasMany(ReceptionItem::class);
    }
}
