<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'id',
        'stockMinimal',
        'quantiteDisponible',
        'depot_id',
        'article_id',
        'commentaires',
    ];

    public function depot()
    {
        return $this->belongsTo(Depot::class);
    }


    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function mouvementStocks()
    {
        return $this->hasMany(MouvementStock::class);
    }
}
