<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lot extends Model
{
    protected $fillable = ['libellelot', 'is_actif'];
    protected $hidden = ['created_at', 'updated_at'];

    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    public function maisons(){
        return $this->hasMany(Maison::class);
    }
}
