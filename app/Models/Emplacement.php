<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emplacement extends Model
{
    protected $fillable = ['nomEmplacement', 'descriptionEmplacement', 'adresseEmplacement', 'chantier_id', 'commentaires'];

    public function chantier()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function stock_emplacements()
    {
        return $this->hasMany(StockEmplacement::class);
    }
}
