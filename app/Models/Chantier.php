<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\RecueilBesoin;
use App\Models\Depot;

class Chantier extends Model
{
    protected $fillable = [
        'code_chantier',
        'denomination',
        'description',
        'longitude',
        'latitude',
        'adresse',
        'chef_chantier_id',
        'type_chantier_id'
    ];


    public function chef_chantier()
    {
        return $this->belongsTo(User::class);
    }

    // public function chefChantier()
    // {
    //     return $this->belongsTo(Employe::class);
    // }

    public function typeChantier()
    {
        return $this->belongsTo(TypeChantier::class);
    }

     public function recueilBesoins()
     {
         return $this->hasMany(RecueilBesoin::class);
     }

    public function enlevement()
    {
        return $this->hasMany(Enlevement::class);
    }

    public function besoinJournaliers()
    {
        return $this->hasMany(BesoinJournalier::class);
    }

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }

    public function sections()
    {
        return $this->hasMany(Section::class);
    }

    public function prefats()
    {
        return $this->hasMany(Prefat::class);
    }


    public function receptions()
    {
        return $this->hasMany(Reception::class);
    }

    public function depots()
    {
        return $this->hasMany(Depot::class);
    }
}
