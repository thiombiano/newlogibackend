<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Maison extends Model
{
    protected $fillable = ['libellemaison', 'description', 'type_maison_id', 'lot_id',
        'technicien_id', 'is_finished', 'societe_id', 'is_actif', 'controleur_id', 'superficie', 'client', 'codevirtuel'];
    protected $hidden = ['created_at', 'updated_at'];

    public function lot(){
        return $this->belongsTo(Lot::class);
    }

    public function technicien(){
        return $this->belongsTo(User::class);
    }

    public function controleur(){
        return $this->belongsTo(User::class);
    }

    public function societe(){
        return $this->belongsTo(SocieteConstructive::class);
    }

    public function type_maison(){
        return $this->belongsTo(TypeMaison::class);
    }

    public function prestataires(){
        return $this->belongsToMany(Prestation::class,'prestations');
    }

    public function etapes(){
        return $this->belongsToMany(EtapeConstruction::class,'prestations');
    }

}
