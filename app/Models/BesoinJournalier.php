<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BesoinJournalier extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['journee_id', 'chantier_id', 'typebesoin', 'technicien_id', 'dateBJ'];

    public function chantier()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'besoin_jitems')->withPivot(['id', 'is_out', 'quantiteBesoin', 'etape_construction_id', 'maison_id', 'prefat_id', 'fonction_id', 'commentaires']);
    }

    public function besoin_jitems()
    {
        return $this->hasMany(BesoinJitem::class);
    }

}
