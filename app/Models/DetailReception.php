<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailReception extends Model
{
    public function detailBesoin()
    {
        return $this->belongsTo(DetailBesoin::class);
    }
}
