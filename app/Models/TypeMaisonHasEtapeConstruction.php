<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeMaisonHasEtapeConstruction extends Model
{
    protected $table ="type_has_etapes";
    public $timestamps = false;
    protected $fillable = ['type_maison_id', 'etape_id'];
}
