<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Fournisseur;

class Commande extends Model
{
    protected $fillable = [
        'id',
        'numero_BC',
        'numero_DA',
        'date_BC',
        'statut_BC',
        'date_lvraison_prevue',
        'chantier_id',
        'fournisseur_id',
        'is_prix_ttc',
        'montant_ttc',
        'montant_tva',
        'montant_bic',
        'mode_paiement',
        'modepaiment_id',
        'is_receptionne',
        'isCanceled',
        'canceledcommentaire',
        'personneacontacter',
        'isreceptionpartiellefinale'

    ];

    public function modepaiment()
    {
        return $this->belongsTo(Modepaiment::class);
    }


    public function chantier()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function fournisseur()
    {
        return $this->belongsTo(Fournisseur::class);
    }

    public function commande_items()
    {
        return $this->hasMany(CommandeItem::class);
    }

    public function receptions()
    {
        return $this->hasMany(Reception::class);
    }
    // public function articles()
    // {
    //     return $this->belongsToMany(Article::class, 'commande_articles')->withPivot(['quantite', 'prixUnitaire', 'commentaires']);
    // }




}
