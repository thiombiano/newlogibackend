<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReceptionItem extends Model
{
    protected $fillable = [
        'reception_id',
        'article_id',
        'commande_item_id',
        'quantite_receptionnee',
        'commentaires'
    ];

    public function reception()
    {
        return $this->belongsTo(Commande::class);
    }
    public function article()
    {
        return $this->belongsTo(Article::class);
    }
    public function commande_item()
    {
        return $this->belongsTo(CommandeItem::class);
    }
}
