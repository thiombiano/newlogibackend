<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['codeEmploye', 'prenom', 'nom', 'tel', 'email', 'cnib', 'fonction_id', 'societe_constructive_id'];

    public function fonction()
    {
        return $this->belongsTo(Fonction::class);
    }

    public function societe()
    {
        return $this->belongsTo(SocieteConstructive::class);
    }

    public function chantiers()
    {
        return $this->hasMany(Chantier::class);
    }

    public function maisons(){
        return $this->belongsToMany(Maison::class,'prestations');
    }



}
