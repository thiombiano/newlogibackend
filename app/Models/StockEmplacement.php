<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockEmplacement extends Model
{
    protected $fillable = ['emplacement_id','article_id', 'stockMinimal', 'quantiteDisponible', 'commentaires'];

    public function emplacement()
    {
        return $this->belongsTo(Emplacement::class);
    }


    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function mouvementStockEmplacements()
    {
        return $this->hasMany(MouvementStockEmplacement::class);
    }
}
