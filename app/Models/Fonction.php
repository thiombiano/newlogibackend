<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fonction extends Model
{
    //
    protected $hidden = ['created_at', 'updated_at'];
    public function employes()
    {
        return $this->hasMany(Employe::class);
    }
}
