<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeMaison extends Model
{
    protected $fillable = ['libelletypemaison', 'description'];
    protected $hidden = ['created_at', 'updated_at'];
    public function maisons(){
        return $this->hasMany(Maison::class);
    }

    public function etape(){
        return $this->belongsToMany(EtapeConstruction::class,'type_has_etapes');
    }
}
