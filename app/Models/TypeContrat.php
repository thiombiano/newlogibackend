<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeContrat extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['libelletypecontrat', 'isVisible'];
}
