<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enlevement extends Model
{
    protected $fillable = ['numero_En', 'date_En', 'statut_En', 'commentaires', 'chantier_id', 'magasinier_id', 'livreur_id'];

    public function chantier()
    {
        return $this->belongsTo(Chantier::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'enlevement_items')->withPivot(['quantiteASortir', 'besoin_jitems_id', 'date_En', 'article_id', 'enlevement_id', 'etape_construction_id', 'maison_id', 'prefat_id']);
    }

    public function enlevement_items()
    {
        return $this->hasMany(Enlevement::class);
    }

    public function besoins()
    {
        return $this->hasMany(BesoinJournalier::class);
    }
}
