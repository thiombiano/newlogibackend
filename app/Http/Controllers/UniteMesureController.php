<?php

namespace App\Http\Controllers;

use App\Models\UniteMesure;
use Illuminate\Http\Request;

class UniteMesureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UniteMesure::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UniteMesure  $uniteMesure
     * @return \Illuminate\Http\Response
     */
    public function show(UniteMesure $uniteMesure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UniteMesure  $uniteMesure
     * @return \Illuminate\Http\Response
     */
    public function edit(UniteMesure $uniteMesure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UniteMesure  $uniteMesure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UniteMesure $uniteMesure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UniteMesure  $uniteMesure
     * @return \Illuminate\Http\Response
     */
    public function destroy(UniteMesure $uniteMesure)
    {
        //
    }
}
