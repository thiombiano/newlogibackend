<?php

namespace App\Http\Controllers;

use App\Models\TypeChantier;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Models\Audit;

class TypeChantierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return TypeChantier[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(TypeChantier $typeChantier)
    {
        return TypeChantier::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $request->validate([
                'code_type_chantier' => 'required',
                'libelle_type_chantier' => 'required'
            ]);
            $typechantier = TypeChantier::create([
                'code_type_chantier' => $request->code_type_chantier,
                'libelle_type_chantier' => $request->libelle_type_chantier,
            ]);

            $lastaudit =  DB::table('audits')->latest()->first();
            $audit = Audit::findOrFail($lastaudit->id);
            $audit->user_id = $request->idutilisateur;
            $audit->user_type = "App\Models\User";
            $audit->save();

            return response([
                'data' => $typechantier
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $typechantier = TypeChantier::findOrFail($id);
        return $typechantier;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $request->validate([
                'code_type_chantier' => 'required',
                'libelle_type_chantier' => 'required'
            ]);

            $typeChantier = TypeChantier::findOrFail($id);

            $typeChantier->code_type_chantier = $request->code_type_chantier;
            $typeChantier->libelle_type_chantier = $request->libelle_type_chantier;
            $typeChantier->save();

            $lastaudit =  DB::table('audits')->latest()->first();
            $audit = Audit::findOrFail($lastaudit->id);
            $audit->user_id = $request->idutilisateur;
            $audit->user_type = "App\Models\User";
            $audit->save();

            return response([
                'data' => $typeChantier
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $typeChantier = TypeChantier::findOrFail($id);
            $typeChantier->delete();
            $lastaudit =  DB::table('audits')->latest()->first();
            $audit = Audit::findOrFail($lastaudit->id);
            $audit->user_id = $request->idutilisateur;
            $audit->user_type = "App\Models\User";
            $audit->save();
            return response([
                'data' =>  'Type chantier supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
