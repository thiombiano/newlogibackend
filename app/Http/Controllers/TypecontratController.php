<?php

namespace App\Http\Controllers;

use App\Models\TypeChantier;
use App\Models\TypeContrat;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TypecontratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TypeContrat $typeContrat)
    {
        return $typeContrat->where('isVisible','=', true)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'prestataire_id' => 'libelletypecontrat',
            ]);
            $typecontrat = new TypeContrat();
            $typecontrat->libelletypecontrat = $request->libelletypecontrat;
            $typecontrat->save();
            return response([
                'data' => $typecontrat
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeContrat  $typeContrat
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $typecontrat = TypeContrat::findOrFail($id);
            return $typecontrat;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeContrat  $typeContrat
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeContrat $typeContrat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeContrat  $typeContrat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypeContrat $typeContrat)
    {
        try {
            $request->validate([
                'prestataire_id' => 'libelletypecontrat',
            ]);
            $typeContrat->libelletypecontrat = $request->libelletypecontrat;
            $typeContrat->save();
            return response([
                'data' => $typeContrat
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeContrat  $typeContrat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $typecontrat = TypeContrat::findOrFail($id);
            $typecontrat->isVisible = false;
            $typecontrat->save();
            return response([
                'data' =>  'Type contrat supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
