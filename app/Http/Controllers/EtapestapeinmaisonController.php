<?php

namespace App\Http\Controllers;

use App\Http\Resources\EtapeConstructionCollection;
use App\Http\Resources\EtapeStateInMaisonCollection;
use App\Models\EtapeStateInMaison;
use App\Models\Imageetape;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class EtapestapeinmaisonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(EtapeStateInMaison $etapeencour)
    {
        return EtapeStateInMaisonCollection::collection($etapeencour->all()) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function countEtapeForMaison($idmaison) {
            try {
                $totaletape = DB::select(DB::raw('SELECT DISTINCT COUNT(*) AS TOTALETAPE FROM etape_constructions ec, type_maisons tm, type_has_etapes the, maisons m WHERE tm.id = the.type_maison_id  AND ec.id = the.etape_construction_id AND m.type_maison_id = tm.id AND m.id = "'.$idmaison.'" '));
                return $totaletape[0]->TOTALETAPE;
            } catch (\Exception $e) {
                return response([
                    'Erreur' => $e
                ], Response::HTTP_NOT_FOUND);
            }
    }

    public function GetNextEtapeForMaison($idmaison, $idetape) {
        try {
            $etape = DB::select(DB::raw('SELECT DISTINCT ec.id FROM etape_constructions ec, type_maisons tm, type_has_etapes the, maisons m WHERE tm.id = the.type_maison_id  AND ec.id = the.etape_construction_id AND m.type_maison_id = tm.id AND m.id = "'.$idmaison.'"  AND ec.id = "'. ($idetape+1) .'"'));
            return $etape[0]->id;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }
//                $lastetatmaison =  EtapeStateInMaison::orderBy('created_at', 'desc')->where('maison_id','=', $idmaison)->first();
//                $lastetatmaison->etapeState = false;
//                $lastetatmaison->datefermetureetape = date('Y-m-d  H:m:s');
//                $lastetatmaison->commentaires = $request->commentaires;
//                if($newetapeformaison === $nombreetape)
//                    $lastetatmaison->isFinished = 1;
//                $lastetatmaison->save();

//$countdata = EtapeStateInMaison::where('maison_id', '=', $idmaison)->count();

//                    else {
//                    $nombreetape = $this->countEtapeForMaison($idmaison);
//                    $newmaisonstate = EtapeStateInMaison::create([
//                    'etapeState' => 0,
//                    'maison_id' => $request->maison_id,
//                    'etape_construction_id' => $request->etape_construction_id,
//                    'dateouvertureetape'  => date('Y-m-d H:m:s', strtotime($request->dateouvertureetape)),
//                    'nombretotaletape' => $nombreetape,
//                    'numeroetapencour' => $request->etape_construction_id,
//                    ]);
//                    return response([
//                    'data' => $newmaisonstate
//                    ], Response::HTTP_CREATED);
//                    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
                $idmaison = $request->maison_id;
                $nombreetape = $this->countEtapeForMaison($idmaison);
                $newetapeformaison = $this->GetNextEtapeForMaison($idmaison, $request->etape_construction_id);
                global $datedebut;
                if( $request->dateouvertureetape)
                    $datedebut= date('Y-m-d', strtotime($request->dateouvertureetape));
                else
                    $datedebut =  date('Y-m-d H:m:s');
                $newmaisonstate = EtapeStateInMaison::create([
                    'etapeState' => 0,
                    'maison_id' => $request->maison_id,
                    'etape_construction_id' => $request->etape_construction_id,
                    'dateouvertureetape'  => $datedebut,
                    'nombretotaletape' => $nombreetape,
                    'numeroetapencour' => $request->etape_construction_id,
                ]);
                return response([
                    'data' => $newmaisonstate
                ], Response::HTTP_CREATED);
        }     catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    public function ReceptionneEtape(Request $request) {
        try {
            global $datefin;
            if( $request->dateouvertureetape)
                $datefin= date('Y-m-d', strtotime($request->datefermetureetape));
            else
                $datefin =  date('Y-m-d H:m:s');
            $etapeencour = EtapeStateInMaison::findOrFail($request->id);
            $etapeencour->etapeState = 1;
            $etapeencour->datefermetureetape =  $datefin;
            $etapeencour->save();
            $length = $request->length;
            for($i=0; $i < $length; $i++) {
                $image[$i] = $request->file('avatar'.$i);
                $filename[$i] = time() . '.' . $image[$i]->getClientOriginalExtension();
                Image::make($image[$i])->save(public_path('img/etapereceptionnee/' . $filename[$i]));
                $newimage = Imageetape::create([
                    'etapeencour_id' => $etapeencour->id,
                    'filename' => 'etapereceptionnee/'. $filename[$i]
                ]);
            }



            return response([
                'data' => $etapeencour
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            throw $e;
        }
    }


    public function ReceptionneDefinitivementEtape(Request $request) {
        try {
            $etapeencour = EtapeStateInMaison::findOrFail($request->id);
            $etapeencour->isFinished = 1;
            $etapeencour->save();
            return response([
                'data' => $etapeencour
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function AnnulerReceptionEtape(Request $request) {
        try {
            $etapeencour = EtapeStateInMaison::findOrFail($request->id);
            $etapeencour->etapeState = 0;
            $etapeencour->datefermetureetape = null;
            $etapeencour->save();
            return response([
                'data' => $etapeencour
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return EtapeStateInMaisonCollection::collection(EtapeStateInMaison::where('maison_id','=', $id)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function edit(EtapeStateInMaison $etapeStateInMaison)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EtapeStateInMaison $etapeStateInMaison)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $etapeencour = EtapeStateInMaison::findOrFail($id);
            $etapeencour->delete();
            return response([
                'data' =>  'Etape supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
