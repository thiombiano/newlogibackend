<?php

namespace App\Http\Controllers;

use App\Http\Resources\RoleCollection;
use App\Models\Role;
use App\Models\RolesHasPermission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Role $role)
    {
        return RoleCollection::collection($role->get()) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libellerole' => 'required',
            ]);
            $role = new Role();
            $role->libellerole = $request->libellerole;
            $role->descriptionrole = $request->descriptionrole;
            $role->save();
            $idrole = $role->id;

            foreach ($request->permissions  as $p ){
                $rolehaspermission = new RolesHasPermission();
                $rolehaspermission->role_id = $idrole;
                $rolehaspermission->permission_id = $p;
                $rolehaspermission->save();
            }

            return response([
                'data' => $role
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($id)
    {
        try {
             return RoleCollection::collection(Role::where('id','=',$id)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        try {
            $request->validate([
                'libellerole' => 'required',
            ]);

            $role->libellerole = $request->libellerole;
            $role->descriptionrole = $request->descriptionrole;
            $role->save();
            $idrole = $role->id;

            DB::table('roles_has_permissions')->where('role_id','=',$role->id)->delete();
            foreach ($request->permissions  as $p ){
                $rolehaspermission = new RolesHasPermission();
                $rolehaspermission->role_id = $idrole;
                $rolehaspermission->permission_id = $p['id'];
                $rolehaspermission->save();
            }
            return response([
                'data' =>  $role
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        try {
            $role->delete();
            return response([
                'data' =>  'Role supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
