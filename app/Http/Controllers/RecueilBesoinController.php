<?php

namespace App\Http\Controllers;

use App\Models\Chantier;
use App\Models\DetailBesoin;
use App\Models\RecueilBesoin;
use App\Notifications\RecueilBesoinNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\RecueilBesoinCollection;
use App\Http\Resources\RecueilBesoinResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpFoundation\Response;

class RecueilBesoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chantier $chantier)
    {
//        return RecueilBesoinCollection::collection(RecueilBesoin::orderBy('id', 'desc')->get());
        return   $chantier->recueilBesoins()->where('isCanceled','=', false)->orderBy('id', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Chantier $chantier)
    {
        DB::beginTransaction();
        try {
            $RB = new RecueilBesoin($request->all());
            $RB->dateRB = date('Y-m-d', strtotime($RB->dateRB));
            $RB->dateLivraisonCible = date('Y-m-d', strtotime($RB->dateLivraisonCible));

            $lastNumeroRB = DB::table('parametres')
                ->Where('code_param', 'RB' . $chantier->code_chantier)
                ->first();

            $recueil_id = DB::table('recueil_besoins')->insertGetId(
                [
                    'numeroRB' => $lastNumeroRB->code_param . str_pad($lastNumeroRB->num_val, 5, '0', STR_PAD_LEFT),
                    'dateRB' =>  $RB->dateRB,
                    'destinations' =>  $RB->destinations,
                    'chantier_id' => $chantier->id,
                    'statutRB' => false,
                    'dateLivraisonCible' => $RB->dateLivraisonCible,
                    'isVisaDemandeur' => false,
                    'isVisaConducteurChantier' => false,
                    'isVisaResponsableStock' => false,
                    'isVisaDirecteurTechnique' => false,
                    'isVisaResponsableAchat'=> false,
                ]
            );


            $articles = $request->articles;
            if ($articles) {
                foreach ($articles as $article) {
                    DB::table('detail_besoins')->insert(
                        [
                            'recueil_besoin_id' => $recueil_id,
                            'article_id' => $article['article_id'],
                            'quantiteDemandee' => $article['quantiteDemandee']
                        ]
                    );
                }
            }

            $user = User::findOrFail(3);

            DB::table('parametres')
                ->Where('code_param', 'RB' . $chantier->code_chantier)
                ->update(['num_val' => $lastNumeroRB->num_val + 1]);

            $details = [

                'greeting' => 'Bonjour / Bonsoir M. '.$user->name,

                'body' => 'Vous avez recu une demande de besoin provenant du chantier '. $chantier->code_chantier,

                'thanks' => 'Merci de vous connectez à la plateforme CHANTI-GEST pour traiter cette demande',

                'actionText' => 'Accéder à CHANTI-GEST',

                'actionURL' => url('http://10.100.35.2'),

                'order_id' => 101

            ];
            Notification::send($user, new RecueilBesoinNotification($details));


            DB::commit();

            return response([
                'data' => new RecueilBesoinResource($RB)
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RecueilBesoin  $recueilBesoin
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recueil = RecueilBesoin::findOrFail($id);
        return new RecueilBesoinResource($recueil);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RecueilBesoin  $recueilBesoin
     * @return \Illuminate\Http\Response
     */
    public function update($recueil_id, Request $request)
    {
        try {
            $recueil = RecueilBesoin::findOrFail($recueil_id);
            $recueil->dateRB = date('Y-m-d', strtotime($request->dateRB));
            $recueil->dateLivraisonCible = date('Y-m-d', strtotime($request->dateLivraisonCible));
            $recueil->destinations = $request->destinations;
            $recueil->save();

            DB::table('detail_besoins')->where('recueil_besoin_id','=', $recueil_id)->delete();

            $articles = $request->articles;
            if ($articles) {

                foreach ($articles as $article) {

                    $data = array(
                        'recueil_besoin_id' => $recueil_id,
                        'article_id' => $article['article_id'],
                        'quantiteDemandee' => $article['quantiteDemandee']
                    );
                    DetailBesoin::updateOrCreate(
                        [
                            'recueil_besoin_id' => $recueil_id,
                            'article_id' => $article['article_id'],
                        ], $data
                    );
                }
            }
            $user = User::findOrFail(3);
            $details = [

                'greeting' => 'Bonjour / Bonsoir M. '.$user->name,

                'body' => 'Vous avez recu une notification pour la mise à jour des info du recueil de besoin suivant : '. $recueil->numeroRB,

                'thanks' => 'Merci de vous connectez à la plateforme CHANTI-GEST pour traiter cette demande',

                'actionText' => 'Accéder à CHANTI-GEST',

                'actionURL' => url('http://10.100.35.2'),

                'order_id' => 101

            ];
            Notification::send($user, new RecueilBesoinNotification($details));
            return  response(DB::table('recueil_besoins')->where('id', $recueil_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function updateforbesoinannulation($recueil_id, Request $request)
    {
        try {
            $recueil=  RecueilBesoin::findOrFail($recueil_id);
            $recueil->canceledcommentaire = $request->canceledcommentaire;
            $recueil->isCanceled = true;
            $recueil->save();
            $user = User::findOrFail(3);
            $details = [

                'greeting' => 'Bonjour / Bonsoir M. '.$user->name,

                'body' => 'Vous avez recu une notification pour annulation du besoin : '. $recueil->numeroRB,

                'thanks' => 'Merci de vous connectez à la plateforme CHANTI-GEST pour traiter cette demande',

                'actionText' => 'Accéder à CHANTI-GEST',

                'actionURL' => url('http://10.100.35.2'),

                'order_id' => 101

            ];
            Notification::send($user, new RecueilBesoinNotification($details));
            return  response(DB::table('recueil_besoins')->where('id','=', $recueil_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }


    public function validerforbesoinannulation($id)
    {
        try {
            $recueil=  RecueilBesoin::findOrFail($id);
            $recueil->isValidate = true;
            $recueil->save();
            $user = User::findOrFail(5);
            $details = [

                'greeting' => 'Bonjour / Bonsoir M. '.$user->name,

                'body' => 'Votre besoin formulé a été validée. Il sagit du  Besoin : '. $recueil->numeroRB,

                'thanks' => 'Merci de vous connectez à la plateforme CHANTI-GEST',

                'actionText' => 'Accéder à CHANTI-GEST',

                'actionURL' => url('http://10.100.35.2'),

                'order_id' => 101

            ];
            Notification::send($user, new RecueilBesoinNotification($details));
            return  response(DB::table('recueil_besoins')->where('id','=', $id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RecueilBesoin  $recueilBesoin
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecueilBesoin $recueilBesoin)
    {
        //
    }
}
