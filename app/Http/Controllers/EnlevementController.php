<?php

namespace App\Http\Controllers;

use App\Http\Resources\BesoinJournalierResource;
use App\Http\Resources\EnlevementResource;
use App\Http\Resources\ReceptionResource;
use App\Models\BesoinJitem;
use App\Models\Chantier;
use App\Models\Commande;
use App\Models\Emplacement;
use App\Models\Enlevement;
use App\Models\Reception;
use App\Models\Stock;
use App\Models\StockEmplacement;
use App\Notifications\ReceptionNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EnlevementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chantier $chantier)
    {
        return EnlevementResource::collection($chantier->enlevement()->orderBy('id', 'desc')->get());
    }

    public function indexenlevementsbyutilisateur($idchantier, $idutilisateur)
    {
        $chantier = Chantier::findOrFail($idchantier);
        return EnlevementResource::collection($chantier->enlevement()->where('magasinier_id','=', $idutilisateur)->orderBy('id', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Chantier $chantier)
    {
        global $is_all_out;
        DB::beginTransaction();
        try {

            $enlevement = new Enlevement($request->all());
            $enlevement->chantier_id = $request->chantier_id;
            $enlevement->date_En = date('d-m-Y H:i:s');
            $enlevement->magasinier_id = $request->magasinier_id;
            $enlevement->typeenlevement = $request->typeenlevement;
            $enlevement->besoin_journalier_id = $request->besoin_journalier_id;
            $enlevement->livreur_id = $request->livreur_id;
            $lastenlevement = DB::table('parametres')
                ->Where('code_param', 'ENLV' . $chantier->code_chantier)
                ->first();

            $enlevement_id = DB::table('enlevements')->insertGetId(
                [
                    'numero_En' => $lastenlevement->code_param . str_pad($lastenlevement->num_val, 5, '0', STR_PAD_LEFT),
                    'besoin_journalier_id' => $enlevement->besoin_journalier_id,
                    'magasinier_id' => $enlevement->magasinier_id,
                    'typeenlevement' => $enlevement->typeenlevement,
                    'livreur_id' => $enlevement->livreur_id,
                    'date_En' => date('Y-m-d H:i:s'),
                    'chantier_id' => $chantier->id
                ]
            );

            if ($enlevement->chantier->depots->count() > 1) {
                $depot_unique = false;
            } else {
                $depot_unique = true;
                $depot = $enlevement->chantier->depots->first();
            }

            $articles = $request->articles;
            if ($articles) {
                foreach ($articles as $article) {
                    DB::table('enlevement_items')->insert(
                        [
                            'enlevement_id' => $enlevement_id,
                            'article_id' => $article['article_id'],
                            'maison_id' => $article['maison_id'],
                            'prefat_id' => $article['prefat_id'],
                            'besoin_jitems_id' => $article['besoin_jitems_id'],
                            'etape_construction_id' => $article['etape_construction_id'],
                            'quantiteASortir' => $article['quantiteASortir'],
                            'date_En' => date('Y-m-d H:i:s'),
                        ]
                    );

                    if( $article['reliquat'] ==  $article['quantiteASortir']) {
                        $is_all_out = true;
                        $data = array(
                            'is_out' => true
                        );

                        BesoinJitem::updateOrCreate([
                            'maison_id' =>  $article['maison_id'],
                            'prefat_id' =>  $article['prefat_id'],
                            'etape_construction_id' => $article['etape_construction_id'],
                            'article_id' => $article['article_id'],
                            'id' => $article['besoin_jitems_id']
                        ], $data );
                    } else {
                        $is_all_out = false;
                    }


                    if ($depot_unique) {
                        #Récuperer le stock
                        $stock = Stock::where('article_id', '=', $article['article_id'])
                            ->where('depot_id', '=', $depot->id)
                            ->first();
                        if ($stock) {
                            # Mettre à jour le stock
                            $stockInitial = $stock->quantiteDisponible;
                            $stock->quantiteDisponible -= $article['quantiteASortir'];
                            $stock->save();

                            DB::table('mouvement_stocks')->insertGetId(
                                [
                                    'stock_id' => $stock->id,
                                    'operation_id' => $enlevement_id,
                                    'operation_type' => 'Enlèvement',
                                    'operation_sens' => 'Sortie',
                                    'quantiteAvant' => $stockInitial,
                                    'quantite' => $article['quantiteASortir'],
                                    'quantiteApres' => $stockInitial - $article['quantiteASortir'],
                                    'created_at' => Carbon::now(),
                                    'date_RC' => date('Y-m-d H:i:s'),
                                ]
                            );

                        }
                    }

                }

                if($is_all_out == true) {
                    DB::table('enlevements')
                        ->where('id', '=', $enlevement_id)
                        ->update(
                            [
                                'statut_En' => true
                            ]
                        );
                }

            }


            DB::table('parametres')
                ->Where('code_param', 'ENLV' . $chantier->code_chantier)
                ->update(['num_val' => $lastenlevement->num_val + 1]);

            DB::commit();
            return response([
                'data' => new EnlevementResource($enlevement)
            ], Response::HTTP_CREATED);

        }  catch (\Exception $e) {
            DB::rollback();
            throw $e;
            }
    }



    public function storeForEnlevementDirectVersMaison(Request $request, $idchantier)
    {
        DB::beginTransaction();
        try {
            $chantier = Chantier::findOrFail($idchantier);
            $enlevement = new Enlevement($request->all());
            $enlevement->chantier_id = $request->chantier_id;
            $enlevement->date_En = date('Y-m-d H:i:s');
            $enlevement->magasinier_id = $request->magasinier_id;
            $enlevement->typeenlevement = "Enlevement vers les maisons";
            $enlevement->besoin_journalier_id = $request->besoin_journalier_id;
            $lastenlevement = DB::table('parametres')
                ->Where('code_param', '=','ENLV' . $chantier->code_chantier)
                ->first();

            $enlevement_id = DB::table('enlevements')->insertGetId(
                [
                    'numero_En' => $lastenlevement->code_param . str_pad($lastenlevement->num_val, 5, '0', STR_PAD_LEFT),
                    'besoin_journalier_id' => $enlevement->besoin_journalier_id,
                    'magasinier_id' => $enlevement->magasinier_id,
                    'typeenlevement' => $enlevement->typeenlevement,
                    'date_En' => $enlevement->date_En,
                    'chantier_id' => $chantier->id
                ]
            );

            if ($enlevement->chantier->depots->count() > 1) {
                $depot_unique = false;
            } else {
                $depot_unique = true;
                $depot = $enlevement->chantier->depots->first();
            }

            $articles = $request->articles;
            if ($articles) {
                foreach ($articles as $article) {
                    DB::table('enlevement_items')->insert(
                        [
                            'enlevement_id' => $enlevement_id,
                            'article_id' => $article['article_id'],
                            'maison_id' => $article['maison_id'],
                            'besoin_jitems_id' => $article['besoin_jitems_id'],
                            'etape_construction_id' => $article['etape_construction_id'],
                            'quantiteASortir' => $article['quantiteASortir'],
                        ]
                    );


                    if ($depot_unique) {
                        #Récuperer le stock
                        $stock = Stock::where('article_id', '=', $article['article_id'])
                            ->where('depot_id', '=', $depot->id)
                            ->first();
                        if ($stock) {
                            # Mettre à jour le stock
                            $stockInitial = $stock->quantiteDisponible;
                            $stock->quantiteDisponible -= $article['quantiteASortir'];
                            $stock->save();

                            DB::table('mouvement_stocks')->insertGetId(
                                [
                                    'stock_id' => $stock->id,
                                    'operation_id' => $enlevement_id,
                                    'operation_type' => 'Enlèvement',
                                    'operation_sens' => 'Sortie',
                                    'quantiteAvant' => $stockInitial,
                                    'quantite' => $article['quantiteASortir'],
                                    'quantiteApres' => $stockInitial - $article['quantiteASortir'],
                                    'created_at' => Carbon::now(),
                                    'date_RC' => date('Y-m-d H:i:s'),
                                ]
                            );
                            DB::table('enlevements')
                                ->where('id', '=', $enlevement_id)
                                ->update(
                                    [
                                        'statut_En' => true
                                    ]
                                );
                        }
                    }

                }

            }


            DB::table('parametres')
                ->Where('code_param', 'ENLV' . $chantier->code_chantier)
                ->update(['num_val' => $lastenlevement->num_val + 1]);

            DB::commit();
            return response([
                'data' => new EnlevementResource($enlevement)
            ], Response::HTTP_CREATED);

        }  catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }


    public function storeEnlevementDirectlyOnPrefat(Request $request, $idchantier)
    {
        DB::beginTransaction();
        try {
            $chantier = Chantier::findOrFail($idchantier);
            $enlevement = new Enlevement($request->all());
            $enlevement->chantier_id = $request->chantier_id;
            $enlevement->date_En = date('Y-m-d H:i:s');
            $enlevement->magasinier_id = $request->magasinier_id;
            $enlevement->besoin_journalier_id = $request->besoin_journalier_id;
            $enlevement->typeenlevement = "Enlevement vers les préfats";
            $lastenlevement = DB::table('parametres')
                ->Where('code_param', '=','ENLV' . $chantier->code_chantier)
                ->first();

            $enlevement_id = DB::table('enlevements')->insertGetId(
                [
                    'numero_En' => $lastenlevement->code_param . str_pad($lastenlevement->num_val, 5, '0', STR_PAD_LEFT),
                    'besoin_journalier_id' => $enlevement->besoin_journalier_id,
                    'magasinier_id' => $enlevement->magasinier_id,
                    'date_En' =>  $enlevement->date_En ,
                    'typeenlevement' => $enlevement->typeenlevement,
                    'chantier_id' => $chantier->id
                ]
            );

            if ($enlevement->chantier->depots->count() > 1) {
                $depot_unique = false;
            } else {
                $depot_unique = true;
                $depot = $enlevement->chantier->depots->first();
            }

            $articles = $request->articles;
            if ($articles) {
                foreach ($articles as $article) {
                    DB::table('enlevement_items')->insert(
                        [
                            'enlevement_id' => $enlevement_id,
                            'article_id' => $article['article_id'],
                            'maison_id' => null,
                            'prefat_id' => $article['prefat_id'],
                            'besoin_jitems_id' => null,
                            'etape_construction_id' => null,
                            'quantiteASortir' => $article['quantiteASortir'],
                        ]
                    );


                    if ($depot_unique) {
                        #Récuperer le stock
                        $stock = Stock::where('article_id', '=', $article['article_id'])
                            ->where('depot_id', '=', $depot->id)
                            ->first();
                        if ($stock) {
                            # Mettre à jour le stock
                            $stockInitial = $stock->quantiteDisponible;
                            $stock->quantiteDisponible -= $article['quantiteASortir'];
                            $stock->save();

                            DB::table('mouvement_stocks')->insertGetId(
                                [
                                    'stock_id' => $stock->id,
                                    'operation_id' => $enlevement_id,
                                    'operation_type' => 'Enlèvement',
                                    'operation_sens' => 'Sortie',
                                    'quantiteAvant' => $stockInitial,
                                    'quantite' => $article['quantiteASortir'],
                                    'quantiteApres' => $stockInitial - $article['quantiteASortir'],
                                    'created_at' => Carbon::now(),
                                    'date_RC' => date('Y-m-d H:i:s'),
                                ]
                            );
                            DB::table('enlevements')
                                ->where('id', '=', $enlevement_id)
                                ->update(
                                    [
                                        'statut_En' => true
                                    ]
                                );
                        }
                    }

                }

            }


            DB::table('parametres')
                ->Where('code_param', 'ENLV' . $chantier->code_chantier)
                ->update(['num_val' => $lastenlevement->num_val + 1]);

            DB::commit();
            return response([
                'data' => new EnlevementResource($enlevement)
            ], Response::HTTP_CREATED);

        }  catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }



//    public function storeEnlevementDirectlyOnPrefat(Request $request, $idchantier) {
//        DB::beginTransaction();
//        try {
//
//                $articles = $request->articles;
//                $idemplacement = Emplacement::select('id')->where('chantier_id', '=', $idchantier)->pluck('id')->first();
//                $emplacement = Emplacement::findOrFail($idemplacement);
//
//                $depot_unique = true;
//
//                if ($articles) {
//                    foreach ($articles as $article) {
//                            if ($depot_unique) {
//                                #Récuperer le stock
//                                $stock = StockEmplacement::where('article_id', '=', $article['article_id'])
//                                    ->where('emplacement_id', '=', $emplacement->id)
//                                    ->first();
//                                if ($stock) {
//                                    # Mettre à jour le stock
//                                    $stockInitial = $stock->quantiteDisponible;
//                                    $stock->quantiteDisponible += $article['quantite'];
//                                    $stock->save();
//
//                                    DB::table('mouvement_stock_emplacements')->insertGetId(
//                                        [
//                                            'stock_emp_id' => $stock->id,
//                                            'operation_id' => null,
//                                            'operation_type' => 'Reception vers prefat',
//                                            'operation_sens' => 'Entrée',
//                                            'quantiteAvant' => $stockInitial,
//                                            'quantite' => $article['quantite'],
//                                            'quantiteApres' => $stockInitial + $article['quantite'],
//                                            'created_at' => Carbon::now(),
//                                            'date_Em' => date('Y-m-d')
//                                        ]
//                                    );
//
//                                }
//                            } else {
//                             return   $request->articles;
//                            }
//                    }
//                }
//
//                DB::commit();
//
//        } catch (\Exception $e) {
//            DB::rollback();
//            throw $e;
//        }
//    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Enlevement  $enlevement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $enlevement = Enlevement::findOrFail($id);
        return new EnlevementResource($enlevement);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Enlevement  $enlevement
     * @return \Illuminate\Http\Response
     */
    public function edit(Enlevement $enlevement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Enlevement  $enlevement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enlevement $enlevement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enlevement  $enlevement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enlevement $enlevement)
    {
        //
    }
}
