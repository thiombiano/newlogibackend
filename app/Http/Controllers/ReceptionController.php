<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\CommandeItem;
use App\Models\Reception;
use App\Models\ReceptionItem;
use Illuminate\Http\Request;
use App\Http\Resources\ReceptionCollection;
use App\Http\Resources\ReceptionResource;
use Illuminate\Support\Facades\DB;

class ReceptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ReceptionCollection::collection(Reception::all());
    }

    public function displayallreceptionpartiellefinale()
    {
        return ReceptionCollection::collection(Reception::where('typedereception', '=', 'Reception partielle finale')->get());
    }

    public function displayallReceptiontotale()
    {
        return ReceptionCollection::collection(Reception::where('typedereception', '=', 'Reception totale')->get());
    }

    public function displayallReceptionpartielle()
    {
        return ReceptionCollection::collection(Reception::where('typedereception', '=', 'Reception partielle')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function show(Reception $reception)
    {
        return new ReceptionResource($reception);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function edit(Reception $reception)
    {
        //
    }

    public function validerreceptionpartiellefinale($idreception){

        try {

        $reception_items = ReceptionItem::where('reception_id', '=', $idreception)->get();
        $idcommande = Reception::select('commande_id')->where('id', '=', $idreception)->pluck('commande_id')->first();
        $reception = Reception::findOrFail($idreception);
        $commande_items = CommandeItem::where('commande_id', '=', $idcommande)->get();
        global $montantttc;
        foreach ($commande_items as $detailc) {
            foreach ($reception_items as $detailr) {
                if($detailc->article_id == $detailr->article_id)
                {
                    $detailc->quantite_commandee = $detailr->quantite_receptionnee;
                    $montantttc += $detailc->quantite_commandee * $detailc->prix_unitaire;
                    $commandes = Commande::findOrFail($detailc->commande_id);
                    $commandes->montant_ttc = $montantttc;
                    $commandes->save();
                    $detailc->save();
                }
            }
        }
            $reception->isValidateForReceptionPFinalle = true;
            $reception->save();
            return new ReceptionResource($reception);

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reception $reception)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reception $reception)
    {
        //
    }
}
