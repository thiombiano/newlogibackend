<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\CommandeResource;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Commande $Commmande)
    {
        return Commande::Where('chantier_id', 1)->orderBy('id', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'numeroBC' => 'required',
                'chantier_id' => 'required',
                'fournisseur_id' => 'required'
            ]);
            $BC = new Commande($request->all());
            //Générer automatiquement le numéro de RB
            $BC->dateBC = \Carbon\Carbon::now()->addDays(2);
            $BC->statutBC = 1;
            $BC->dateLivraisonCible = \Carbon\Carbon::now()->addDays(7);
            $BC->save();

            // $besoins = $request->besoins;
            // if ($besoins) {
            //     $BC->articles()->sync($besoins);
            //     // foreach ($besoins as $besoin) {
            //     //     $RB->articles()->attach($besoin['id_article'], ['quantiteDemandee' => $besoin['quantiteDemandee'], 'maison_id' => $besoin['maison_id'],'commentaires'=>$besoin['commentaires']]);
            //     // }
            // }
            return response($BC, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function show(Commande $commande)
    {
        return new CommandeResource($commande) ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function edit(Commande $commande)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commande $commande)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commande $commande)
    {
        //
    }
}
