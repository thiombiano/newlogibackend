<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Chantier;
use App\Models\Depot;
use App\Models\Stock;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\ChantierCollection;
use App\Http\Resources\ChantierResource;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

class ChantierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Chantier[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Chantier $chantier)
    {
        return Chantier::with('chef_chantier')->where('is_actif', '=', 1)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $request->validate([
                'code_chantier' => 'required',
                'denomination' => 'required'
            ]);
            $chantier = Chantier::create([
                'code_chantier' => $request->code_chantier,
                'denomination' => $request->denomination,
                'description' => $request->description,
                'adresse' => $request->adresse,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'chef_chantier_id' => $request->chef_chantier_id,
                'type_chantier_id' => $request->type_chantier_id
            ]);

            if($chantier) {
                $nb_depot = Depot::count();
                $depot = Depot::create([
                    'nomDepot' => 'Depot '. $chantier->id,
                    'descriptionDepot' => 'Depot '.$nb_depot. ' '. $chantier->denomination,
                    'chantier_id' => $chantier->id
                ]);

                $articles = Article::all();
                $depotId =  $depot->id;
                //Initialiser le stock

                foreach ($articles as $article) {
                    \Log::info($article->id);
                    $stock = Stock::create([
                        'depot_id' => $depotId,
                        'article_id'  => $article->id,
                        'stockMinimal' => 0,
                        'quantiteDisponible' => 0,
                        'commentaires' => ''
                    ]);
                    \Log::info($stock->id);
                }

            }


            $image = $request->file('url_photo');
            if(isset($image))
            {

                $image = $request->file('url_photo');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save( public_path('img/chantier/' . $filename ) );
                $chemin='chantier/';
                $chantier->url_photo = $chemin.$filename;


                $chantier->save();
            }
            else
            {
                $filename = 'PhotoChantiers.jpg';
                $chemin='chantier/';
                $chantier->url_photo = $chemin.$filename;
                $chantier->save();
            }

            return response([
                'data' => $chantier
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chantier  $chantier
     * @return \App\Chantier|Chantier
     */
    public function show($id)
    {
        try {
            return Chantier::with('chef_chantier')->where('id','=', $id)->first();
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chantier  $chantier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chantier $chantier)
    {
        try {
            $request->validate([
                'code_chantier' => 'required',
                'denomination' => 'required'
            ]);

            global $isPasswordChange ;
            $imageCourantBD = $chantier->url_photo;

            global $filename;

            if($request->url_photo != null && $request->file('url_photo'))
            {
                $image = $request->file('url_photo');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save( public_path('img/chantier/' . $filename ) );

            }
            else
                $filename='';

            $chemin='chantier/';
            $imageCourantAPP = $chemin.''.$filename;

            if($request->password == '')
                $isPasswordChange= false;
            else
                $isPasswordChange=true;


            $chantier->code_chantier = $request->code_chantier;
            $chantier->denomination = $request->denomination;
            $chantier->description = $request->description;
            $chantier->adresse = $request->adresse;
            $chantier->longitude = $request->longitude;
            $chantier->latitude = $request->latitude;
            $chantier->chef_chantier_id = $request->chef_chantier_id;
            $chantier->type_chantier_id = $request->type_chantier_id;
            $chantier->url_photo = ($filename === '' ? $imageCourantBD : $imageCourantAPP);
            $chantier->save();


            return response([
                'data' =>  $chantier
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chantier  $chantier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chantier $chantier)
    {
        if ($chantier)
            $chantier->is_actif = false;
            $chantier->save();

        return response(['data' => 'Chantier supprimé'], Response::HTTP_NO_CONTENT);
    }
}
