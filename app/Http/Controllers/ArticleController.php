<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\ArticleCollection;
use App\Models\Depot;
use App\Models\Stock;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Models\Audit;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Article::with('unite_mesure')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libelle_article' => 'required',
                'reference'=> 'unique:articles',
            ]);

            $article = new Article($request->all());
            $article->unite_mesure_id = $request->unite_mesure_id;
            $article->save();

            foreach (Depot::all() as $depot) {
                $stock = Stock::create([
                    'depot_id' => $depot->id,
                    'article_id'  => $article->id,
                    'stockMinimal' => 0,
                    'quantiteDisponible' => 0,
                    'commentaires' => ''
                ]);
            }

            $lastaudit =  DB::table('audits')->latest()->first();
            $audit = Audit::findOrFail($lastaudit->id);
            $audit->user_id = $request->id;
            $audit->user_type = "App\Models\User";
            $audit->save();

            return response([
                'data' => $article
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        try {
            return $article;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        try {

            $request->validate([
                'libelle_article' => 'required',
                'reference'=> 'unique:articles,id',
            ]);

            $article->update($request->all());
//            $lastaudit =  DB::table('audits')->latest()->first();
//            $audit = Audit::findOrFail($lastaudit->id);
//            $audit->user_id = $request->idutilisateur;
//            $audit->user_type = "App\Models\User";
//            $audit->save();
            return response([
                'data' =>  $article
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Article $article)
    {
        try {
            $article->delete();
            $lastaudit =  DB::table('audits')->latest()->first();
            $audit = Audit::findOrFail($lastaudit->id);
            $audit->user_id = $request->idutilisateur;
            $audit->user_type = "App\Models\User";
            $audit->save();
            return response([
                'data' =>  'Article supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }

    }
}
