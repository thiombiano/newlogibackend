<?php

namespace App\Http\Controllers;

use App\Models\MouvementStock;
use Illuminate\Http\Request;

class MouvementStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MouvementStock::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MouvementStock  $mouvementStock
     * @return \Illuminate\Http\Response
     */
    public function show(MouvementStock $mouvementStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MouvementStock  $mouvementStock
     * @return \Illuminate\Http\Response
     */
    public function edit(MouvementStock $mouvementStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MouvementStock  $mouvementStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MouvementStock $mouvementStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MouvementStock  $mouvementStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(MouvementStock $mouvementStock)
    {
        //
    }
}
