<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\Chantier;
use App\Models\Article;
use App\Models\MouvementStock;
use App\Http\Resources\MouvementStockCollection;

class ChantierArticleMouvementStock extends Controller
{
    public function show(int $chantier, int $article)
    {
        \Log::warning('message');
        $mouvementStock = MouvementStock::with('stock')->orderBy('id', 'desc');
        $mouvementStock = $mouvementStock->whereHas('stock', function ($query) use ($article) {
            $query->where('article_id', $article);
        })->get();
        return MouvementStockCollection::collection($mouvementStock);
    }
    // public function index(int $chantier, int $article)
    // {
    //     \Log::warning('message' + $article);
    //     $mouvementStock = MouvementStock::with('stock');
    //     $mouvementStock = $mouvementStock->whereHas('stock', function ($query) use ($article) {
    //         $query->where('article_id', $article);
    //     })->get();
    //     $mouvementStock = $mouvementStock;
    //     return MouvementStockCollection::collection($mouvementStock);
    // }
}
