<?php

namespace App\Http\Controllers;

use App\Http\Resources\SectionResource;
use App\Models\Chantier;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return SectionResource
     */
    public function index(Chantier $chantier)
    {
        return   $chantier->sections()->with('lots')->where('is_actif','=', true)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getlotbysection($idsection) {
        try {
            return DB::select(DB::raw('select l.id, l.libellelot, l.is_actif, l.section_id from sections s, lots l where s.id = l.section_id and l.is_actif = true and s.id="'.$idsection.'"'));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libellesection' => 'required',
            ]);

            $section = new Section($request->all());
            $section->chantier_id = $request->chantier_id;
            $section->save();
            return response([
                'data' => $section
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Chantier $chantier, $id)
    {
        try {
            return   $chantier->sections()
                ->with('lots')
                ->where('is_actif','=', true)
                ->where('id','=',$id)->get();
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        try {
            $request->validate([
                'libellesection' => 'required',
            ]);
            $section = Section::findOrFail($id);
            $section->libellesection = $request->libellesection;
            $section->chantier_id = $request->chantier_id;
            $section->save();
            return response([
                'data' =>  $section
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section = Section::findOrFail($id);
        if ($section)
            $section->is_actif = false;
        $section->save();

        return response(['data' => $section], Response::HTTP_OK);
    }
}
