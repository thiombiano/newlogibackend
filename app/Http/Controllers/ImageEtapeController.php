<?php

namespace App\Http\Controllers;

use App\Models\EtapeStateInMaison;
use App\Models\Imageetape;
use Illuminate\Http\Request;

class ImageEtapeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Imageetape::where('etapeencour_id', '=', $id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function edit(EtapeStateInMaison $etapeStateInMaison)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EtapeStateInMaison $etapeStateInMaison)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EtapeStateInMaison  $etapeStateInMaison
     * @return \Illuminate\Http\Response
     */
    public function destroy(EtapeStateInMaison $etapeStateInMaison)
    {
        //
    }
}
