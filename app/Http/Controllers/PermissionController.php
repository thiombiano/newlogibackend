<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Permission[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Permission::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libellepermission' => 'required',
            ]);

            $permission = new Permission($request->all());
            $permission->save();
            return response([
                'data' => $permission
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Permission
     */
    public function show(Permission $permission)
    {
        try {
            return $permission;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        try {
            $request->validate([
                'libellepermission' => 'required',
            ]);

            $permission->update($request->all());
            return response([
                'data' =>  $permission
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        try {
            $permission->delete();
            return response([
                'data' =>  'Permission supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
