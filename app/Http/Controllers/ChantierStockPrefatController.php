<?php

namespace App\Http\Controllers;

use App\Http\Resources\StockEmplacementCollection;
use App\Models\Chantier;
use App\Models\StockEmplacement;
use Illuminate\Http\Request;

class ChantierStockPrefatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chantier $chantier)
    {
        $stocksprefat = StockEmplacement::whereHas('emplacement', function ($query) use ($chantier) {
            $query->where('chantier_id', '=', $chantier->id);
        })->get();
        return  StockEmplacementCollection::collection($stocksprefat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Chantier  $chantier
     * @return \Illuminate\Http\Response
     */
    public function show(Chantier $chantier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Chantier  $chantier
     * @return \Illuminate\Http\Response
     */
    public function edit(Chantier $chantier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Chantier  $chantier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chantier $chantier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Chantier  $chantier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chantier $chantier)
    {
        //
    }
}
