<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Chantier;
use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Resources\StockCollection;

class ChantierStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chantier $chantier)
    {
        $stocks = Stock::whereHas('depot', function ($query) use ($chantier) {
            $query->where('chantier_id', '=', $chantier->id);
        })->get();

        return StockCollection::collection($stocks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(int $chantier, int $article)
    {
        \Log::Info('Article Id: ' . $article);
        \Log::Info('Chantier Id: ' . $chantier);

        $stock = Stock::where('article_id', $article)->whereHas('depot', function ($query) use ($chantier) {
            $query->where('chantier_id', '=', $chantier);
        })->first();


        return $stock;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }
}
