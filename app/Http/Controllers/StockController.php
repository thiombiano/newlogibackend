<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use Illuminate\Http\Request;
use App\Http\Resources\StockCollection;
use App\Models\Article;
use App\Models\Depot;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Mime\Encoder\QpContentEncoder;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StockCollection::collection(Stock::all());
    }

    public function getQuantiteDisponibleByIDarticleAndIDdepot($idchantier, $idarticle) {
        $iddepot = [];
        $iddepot = DB::select(DB::raw('SELECT ID AS id FROM depots d WHERE d.chantier_id = "'. $idchantier . '" '));
        $quantiteDisponible =  DB::select(DB::raw('SELECT s.quantiteDisponible FROM stocks s WHERE s.depot_id = "'. $iddepot[0]->id . '" AND s.article_id = "'. $idarticle .'" '));
       return  $quantiteDisponible[0]->quantiteDisponible;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Log::info('Entrée');
        $articles = Article::all();
        $depotId =  $request->depot_id;
        //Initialiser le stock

            foreach ($articles as $article) {
                \Log::info($article->id);
                $stock = Stock::create([
                    'depot_id' => $depotId,
                    'article_id'  => $article->id,
                    'stockMinimal' => 0,
                    'quantiteDisponible' => 0,
                    'commentaires' => ''
                ]);
                \Log::info($stock->id);
            }

        return  'Initialisation stocks: OK';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }
}
