<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommandeItemCollection;
use App\Models\Commande;
use App\Models\CommandeItem;
use Illuminate\Http\Request;

class CommandeItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CommandeItem[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Commande $commande)
    {
        return new CommandeItemCollection($commande->commande_items()->get()) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommandeItem  $commandeItem
     * @return \Illuminate\Http\Response
     */
    public function show(CommandeItem $commandeItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommandeItem  $commandeItem
     * @return \Illuminate\Http\Response
     */
    public function edit(CommandeItem $commandeItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommandeItem  $commandeItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommandeItem $commandeItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommandeItem  $commandeItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommandeItem $commandeItem)
    {
        //
    }
}
