<?php

namespace App\Http\Controllers;

use App\Models\Modepaiment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ModepaiementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Modepaiment::get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libellemodepaiement' => 'required',
            ]);

            $modepaiement= new Modepaiment($request->all());
            $modepaiement->save();


            return response([
                'data' => $modepaiement
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Modepaiment
     */
    public function show(Modepaiment $modepaiment)
    {
        try {
//            $modepaiment = Modepaiment::findOrFail($id);
            return $modepaiment;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Modepaiment $modepaiment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modepaiment $modepaiment)
    {
        try {

            $request->validate([
                'libellemodepaiement' => 'required',
            ]);

            $modepaiment->update($request->all());
            return response([
                'data' =>  $modepaiment
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Modepaiment $modepaiment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modepaiment $modepaiment)
    {
        try {
            $modepaiment->delete();
            return response([
                'data' =>  'Mode paiement supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
