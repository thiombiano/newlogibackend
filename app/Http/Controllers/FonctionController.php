<?php

namespace App\Http\Controllers;

use App\Models\Fonction;
use App\Models\Fournisseur;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class FonctionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Fonction $fonction)
    {
        return $fonction->get();
    }


    public function getlistefonctionbyidmaison($idmaison) {
        try {
            return DB::select(DB::raw('select DISTINCT f.id, f.libelleFonction from prestations p, fonctions f, employes e, maisons m where p.prestataire_id = e.id AND f.id = e.fonction_id AND m.id = p.maison_id AND m.id = "'.$idmaison.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }


    public function getlistefonctionbyidprefat($idprefat) {
        try {
            return DB::select(DB::raw('select DISTINCT f.id, f.libelleFonction from prestations p, fonctions f, employes e, prefats pre where p.prestataire_id = e.id AND f.id = e.fonction_id AND pre.id = p.prefat_id AND pre.id = "'.$idprefat.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getlisteprestatairebyidmaisonandidfonction($idmaison, $idfonction) {
        try {
            return DB::select(DB::raw('select DISTINCT e.nom, e.prenom from prestations p, fonctions f, employes e, maisons m where p.prestataire_id = e.id AND f.id = e.fonction_id AND m.id = p.maison_id AND m.id = "'.$idmaison.'" AND f.id = "'.$idfonction.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getlisteprestatairebyidprefatandidfonction($idprefat, $idfonction) {
        try {
            return DB::select(DB::raw('select DISTINCT e.nom, e.prenom from prestations p, fonctions f, employes e, prefats pre where p.prestataire_id = e.id AND f.id = e.fonction_id AND pre.id = p.prefat_id AND pre.id = "'.$idprefat.'" AND f.id = "'.$idfonction.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'codeFonction' => 'required',
                'libelleFonction' => 'required'
            ]);
            $fonction = new Fonction();
            $fonction->libelleFonction = $request->libelleFonction;
            $fonction->codeFonction = $request->codeFonction;
            $fonction->save();
            return response([
                'data' => $fonction
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fonction  $fonction
     * @return \Illuminate\Http\Response
     */
    public function show(Fonction $fonction)
    {
        try {
            return $fonction;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fonction  $fonction
     * @return \Illuminate\Http\Response
     */
    public function edit(Fonction $fonction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fonction  $fonction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'libelleFonction' => 'required',
            ]);
            $fonction = Fonction::findOrFail($id);
            $fonction->libelleFonction = $request->libelleFonction;
            $fonction->codeFonction = $request->codeFonction;
            $fonction->save();
            return response([
                'data' =>  $fonction
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fonction  $fonction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $fonction = Fonction::findOrFail($id);
            $fonction->delete();
            return response([
                'data' =>  'Fonction supprimée avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
