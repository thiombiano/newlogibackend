<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\CommandeItem;
use App\Models\Fournisseur;
use App\Models\Reception;
use App\Models\ReceptionItem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class FournisseurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Fournisseur::all();
    }

    public function listedesfournisseurnonblacklister()
    {
        return Fournisseur::where('is_canceled', '=', 0)->get();
    }

//    public function rp() {
//        global $receptionpartielleexist;
//        $receptionpartielleexist  = 'On a pas de  receptions partielles en attente';
//        $reception_partielle = Reception::where('typedereception', '=', 'Reception partielle')->get();
//        foreach ($reception_partielle as $rp) {
//            $reception_items = ReceptionItem::where('reception_id', '=', $rp->id)->get();
//            $idcommande = Reception::select('commande_id')->where('id', '=', $rp->id)->pluck('commande_id')->first();
//            $commande = Commande::findOrFail($idcommande);
//            echo $commande->is_receptionne . '<br/>';
//            if($commande->is_receptionne != 1) {
//                $commande_items = CommandeItem::where('commande_id', '=', $idcommande)->get();
//                $quantiterecp = 0;
//                foreach ($commande_items as $detailc) {
//                    foreach ($reception_items as $detailr) {
//                        if($detailc->article_id == $detailr->article_id)
//                        {
//                            if($detailc->quantite_commandee != $detailr->quantite_receptionnee) {
//                                $receptionpartielleexist = 'On a  des receptions partielles en attente';
//                                break;
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        echo $receptionpartielleexist;
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'raison_social' => 'required',
            ]);

            $fournisseur = new Fournisseur($request->all());
            $fournisseur->save();
            return response([
                'data' => $fournisseur
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function show(Fournisseur $fournisseur)
    {
        try {
            return $fournisseur;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function edit(Fournisseur $fournisseur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fournisseur $fournisseur)
    {
        try {
                $request->validate([
                'raison_social' => 'required',
            ]);

            $fournisseur->update($request->all());
            return response([
                'data' =>  $fournisseur
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fournisseur  $fournisseur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Fournisseur $fournisseur)
    {
        try {
            $fournisseur->delete();
            return response([
                'data' =>  'Fournisseur supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    public function blacklisterfournisseur($fournisseur_id, Request $request)
    {
        try {
            $fournisseur=  Fournisseur::findOrFail($fournisseur_id);
            $fournisseur->commentforcanceled = $request->commentforcanceled;
            $fournisseur->is_canceled = true;
            $fournisseur->save();
            return  response(DB::table('fournisseurs')->where('id','=', $fournisseur_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function reactiverfournisseur($fournisseur_id, Request $request)
    {
        try {
            $fournisseur=  Fournisseur::findOrFail($fournisseur_id);
            $fournisseur->commentforcanceled = $request->commentforcanceled;
            $fournisseur->is_canceled = false;
            $fournisseur->save();
            return  response(DB::table('fournisseurs')->where('id','=', $fournisseur_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
