<?php

namespace App\Http\Controllers;

use App\Models\Maison;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class MaisonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Maison::with('type_maison', 'lot','technicien')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getmaisonbysection($section) {
        try {

            return DB::select(DB::raw('select * from maisons m, lots l, sections s where m.lot_id = l.id  and s.id = l.section_id and s.id = "'.$section.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libellemaison' => 'required',
            ]);

            $maison = new Maison($request->all());
            $maison->technicien_id = $request->technicien_id;
            $maison->controleur_id = $request->controleur_id;
            $maison->save();
            return response([
                 $maison->id
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Maison  $maison
     * @return Maison[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function show($id)
    {
        try {
            $maison = Maison::with('type_maison', 'lot','technicien', 'controleur')->where('id', '=', $id)->get();
            return $maison;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function showmaisonbymaisonid($id)
    {
        try {
            $maison =  DB::select(DB::raw('select m.libellemaison, l.libellelot, s.libellesection from maisons m, lots l, sections s where m.lot_id = l.id  and s.id = l.section_id and m.id = "'.$id.'" '));
        return collect($maison) ;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Maison  $maison
     * @return \Illuminate\Http\Response
     */
    public function edit(Maison $maison)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Maison  $maison
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Maison $maison)
    {
        try {
            $request->validate([
                'libellemaison' => 'required',
            ]);

            $maison->update($request->all());
            return response([
                'data' =>  $maison
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Maison  $maison
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $maison = Maison::findOrFail($id);
            $maison->is_actif = false;
            $maison->save();
            return response([
                'data' =>  $maison
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
