<?php

namespace App\Http\Controllers;

use App\Models\Reception;
use App\Models\Article;
use App\Models\Commande;
use App\Models\ReceptionItem;
use App\Notifications\ReceptionNotification;
use App\Notifications\RecueilBesoinNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\ReceptionResource;
use App\Models\Stock;
use App\Models\MouvementStock;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use League\Flysystem\Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Carbon\Carbon;

class CommandeReceptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Commande $commande)
    {
        return  $commande->receptions()->orderBy('date_RC', 'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request, Commande $commande)
    // {
    //     DB::beginTransaction();
    //     $out = new \Symfony\Component\Console\Output\ConsoleOutput();
    //     try {
    //         $reception = new Reception($request->all());
    //         //$reception->dateBC = \Carbon\Carbon::parse(date_format($reception->dateBC,'d/m/Y H:i:s'));
    //         $reception->date_RC = Carbon::createFromFormat('d/m/Y', $reception->date_RC);
    //         //$reception->dateLivraisonCible = \Carbon\Carbon::now()->addDays(7);

    //         $reception->statutRC = 1;
    //         // $commande->receptions()->save($reception);

    //         $lastNumeroRC = DB::table('parametres')
    //             ->Where('code_param', 'BR' . $commande->chantier->code_chantier)
    //             ->first();

    //         \Log::Info($lastNumeroRC->code_param . $lastNumeroRC->num_val);

    //         $reception_id = DB::table('receptions')->insertGetId(
    //             [
    //                 'numero_RC' =>  $lastNumeroRC->code_param . str_pad($lastNumeroRC->num_val, 5, '0', STR_PAD_LEFT),
    //                 'date_RC' =>  $reception->date_RC,
    //                 'statut_RC' => $request->statut_RC,
    //                 'chantier_id' => $request->chantier_id,
    //                 'commande_id' => $request->commande_id,
    //                 'fournisseur_id' => $request->fournisseur_id,
    //                 'livreur' => $request->livreur,
    //                 'telephone_livreur' => $request->telephone_livreur,
    //                 'numero_piece_fournisseur' => $request->numero_piece_fournisseur,
    //                 'commentaires' =>  $request->commentaires,
    //                 'created_at' =>  Carbon::now()
    //             ]
    //         );

    //         $articles = $request->articles;
    //         // if ($articles) {

    //         //     $reception->articles()->sync($articles);
    //         //     //$commande->receptions()->save($reception);

    //         //     foreach ($reception->articles()->get() as $receptionArticle) {
    //         //         // // \Log::info('Log: '.$receptionArticle->pivot->article_id. ': '.$receptionArticle->pivot->quantite. ' - '.$receptionArticle->pivot->prixUnitaire);
    //         //         // $article = Article::find($receptionArticle->pivot->article_id);
    //         //         // // \Log::info('Article stock avant: '.$article->stock);
    //         //         // $article->stock = $article->stock + $receptionArticle->pivot->quantite;
    //         //         // //\Log::info('Article stock aprés: '.$article->stock);
    //         //         // $article->save();

    //         //         # Retouver le stock
    //         //         $stock = Stock::where('article_id', '=', $receptionArticle->pivot->article_id)->first();
    //         //         if ($stock) {
    //         //             # Mettre à jour le stock
    //         //             $stockInitial = $stock->quantiteDisponible;
    //         //             $stock->quantiteDisponible += $receptionArticle->pivot->quantite;
    //         //             $stock->save();
    //         //             # Ajouter l'historisation du Mouvement de stock
    //         //             $mouvementStock = new MouvementStock();
    //         //             $mouvementStock->stock_id =  $stock->id;
    //         //             $mouvementStock->operation_id =  $receptionArticle->pivot->id;
    //         //             $mouvementStock->operation_type = 'Reception';
    //         //             $mouvementStock->operation_sens = 'Entree';
    //         //             $mouvementStock->quantiteAvant = $stockInitial;
    //         //             $mouvementStock->quantite =  $receptionArticle->pivot->quantite;
    //         //             $mouvementStock->quantiteApres = $stockInitial + $receptionArticle->pivot->quantite;
    //         //             $mouvementStock->commentaires =  '';
    //         //             $mouvementStock->save();
    //         //             #
    //         //         }
    //         //     }
    //         // }
    //         // foreach ($articles as $receptionArticle) {
    //         //     \Log::info('Log: '.$receptionArticle[0]);
    //         // }
    //         // $commande->statutBC = 2;
    //         // $commande->save();
    //         // $commande->receptions()->save($reception);
    //         #################################"
    //         #Verifier si le chantier dispose d'un ou plusieurs dépots, au premier cas
    //         # procéder directement à l'entreposage de la reception
    //         #################################
    //         if ($commande->chantier->depots->count() > 1) {
    //             $depot_unique = false;
    //         } else {
    //             $depot_unique = true;
    //             $depot = $commande->chantier->depots->first();
    //         }

    //         if ($articles) {
    //             foreach ($articles as $article) {
    //                 // \Log::info('Aricles: ' . $articles);
    //                 $commande_item = $commande->commande_items
    //                     ->where('article_id', '=', $article['article_id'])
    //                     ->first();

    //                 if ($commande_item) {
    //                     $id_reception_item = DB::table('reception_items')->insertGetId(
    //                         [
    //                             'reception_id' => $reception_id,
    //                             'article_id' => $article['article_id'],
    //                             'commande_item_id' => $commande_item['id'],
    //                             'quantite_receptionnee' => $article['quantite_receptionnee'],
    //                             'commentaires' => $article['commentaires'],
    //                             'created_at' =>  Carbon::now(),
    //                             'date_RC' =>  $reception->date_RC,
    //                         ]
    //                     );

    //                     if ($depot_unique) {
    //                         #Récuperer le stock
    //                         $stock = Stock::where('article_id', '=', $article['article_id'])
    //                             ->where('depot_id', '=', $depot->id)
    //                             ->first();
    //                         if ($stock) {
    //                             # Mettre à jour le stock
    //                             $stockInitial = $stock->quantiteDisponible;
    //                             $stock->quantiteDisponible += $article['quantite_receptionnee'];
    //                             $stock->save();
    //                             # Ajouter l'historisation du Mouvement de stock
    //                             // $mouvementStock = new MouvementStock();
    //                             // $mouvementStock->stock_id =  $stock->id;
    //                             // $mouvementStock->operation_id =  $id_reception_item;
    //                             // $mouvementStock->operation_type = 'Reception';
    //                             // $mouvementStock->operation_sens = 'Entree';
    //                             // $mouvementStock->quantiteAvant = $stockInitial;
    //                             // $mouvementStock->quantite =  $article['quantite_receptionnee'];
    //                             // $mouvementStock->quantiteApres = $stockInitial + $article['quantite_receptionnee'];
    //                             // $mouvementStock->commentaires =  '';
    //                             // $mouvementStock->save();

    //                             DB::table('mouvement_stocks')->insertGetId(
    //                                 [
    //                                     'stock_id' => $stock->id,
    //                                     'operation_id' => $id_reception_item,
    //                                     'operation_type' => 'Reception',
    //                                     'operation_sens' => 'Entrée',
    //                                     'quantiteAvant' => $stockInitial,
    //                                     'quantite' => $article['quantite_receptionnee'],
    //                                     'quantiteApres' => $stockInitial + $article['quantite_receptionnee'],
    //                                     'created_at' =>  Carbon::now(),
    //                                     'date_RC' =>  $reception->date_RC,
    //                                 ]
    //                             );
    //                             DB::table('receptions')
    //                                 ->where('id', '=', $reception_id)
    //                                 ->update(
    //                                     [
    //                                         'statut_RC' => 2
    //                                     ]
    //                                 );
    //                         }
    //                     }
    //                 } else {
    //                     throw new HttpException(404, 'Cet article n\'a pas été commandé');
    //                 }
    //             }
    //         }

    //         DB::table('parametres')
    //             ->Where('code_param', 'BR' . $commande->chantier->code_chantier)
    //             ->update(['num_val' => $lastNumeroRC->num_val + 1]);


    //         DB::commit();
    //         return new ReceptionResource($reception);
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         throw $e;
    //     }
    // }




    public function store(Request $request, Commande $commande)
    {
        DB::beginTransaction();
        $out = new \Symfony\Component\Console\Output\ConsoleOutput();
        try {
            if($request->typedereception != "Reception non conforme") {
            $reception = new Reception($request->all());
            //$reception->dateBC = \Carbon\Carbon::parse(date_format($reception->dateBC,'d/m/Y H:i:s'));
//            $reception->date_RC = date('Y-m-d', strtotime($reception->date_RC));
            $reception->date_RC = date('Y-m-d');
            //$reception->dateLivraisonCible = \Carbon\Carbon::now()->addDays(7);

            $reception->statutRC = 1;
            // $commande->receptions()->save($reception);

            $lastNumeroRC = DB::table('parametres')
                ->Where('code_param', 'BR' . $commande->chantier->code_chantier)
                ->first();

            global $receptionresult, $isnonconforme;
            if($request->typedereception == "Reception non conforme")
            {
                $isnonconforme = true;
            } else {
                $isnonconforme = false;
            }

            \Log::Info($lastNumeroRC->code_param . $lastNumeroRC->num_val);

            $reception_id = DB::table('receptions')->insertGetId(
                [
                    'numero_RC' =>  $lastNumeroRC->code_param . str_pad($lastNumeroRC->num_val, 5, '0', STR_PAD_LEFT),
                    'date_RC' =>  $reception->date_RC,
                    'statut_RC' => $request->statut_RC,
                    'chantier_id' => $request->chantier_id,
                    'commande_id' => $request->commande_id,
                    'fournisseur_id' => $request->fournisseur_id,
                    'livreur' => $request->livreur,
                    'immatriculation' => $request->immatriculation,
                    'telephone_livreur' => $request->telephone_livreur,
                    'numero_piece_fournisseur' => $request->numero_piece_fournisseur,
                    'commentaires' =>  $request->commentaires,
                    'typedereception' =>  $request->typedereception,
                    'isCanceled' =>  $isnonconforme,
                    'canceledcommentaire' =>  $request->canceledcommentaire,
                    'motifpasimmatriculation' =>  $request->motifpasimmatriculation,
                    'created_at' =>  Carbon::now()
                ]
            );

            $articles = $request->articles;
            // if ($articles) {

            //     $reception->articles()->sync($articles);
            //     //$commande->receptions()->save($reception);

            //     foreach ($reception->articles()->get() as $receptionArticle) {
            //         // // \Log::info('Log: '.$receptionArticle->pivot->article_id. ': '.$receptionArticle->pivot->quantite. ' - '.$receptionArticle->pivot->prixUnitaire);
            //         // $article = Article::find($receptionArticle->pivot->article_id);
            //         // // \Log::info('Article stock avant: '.$article->stock);
            //         // $article->stock = $article->stock + $receptionArticle->pivot->quantite;
            //         // //\Log::info('Article stock aprés: '.$article->stock);
            //         // $article->save();

            //         # Retouver le stock
            //         $stock = Stock::where('article_id', '=', $receptionArticle->pivot->article_id)->first();
            //         if ($stock) {
            //             # Mettre à jour le stock
            //             $stockInitial = $stock->quantiteDisponible;
            //             $stock->quantiteDisponible += $receptionArticle->pivot->quantite;
            //             $stock->save();
            //             # Ajouter l'historisation du Mouvement de stock
            //             $mouvementStock = new MouvementStock();
            //             $mouvementStock->stock_id =  $stock->id;
            //             $mouvementStock->operation_id =  $receptionArticle->pivot->id;
            //             $mouvementStock->operation_type = 'Reception';
            //             $mouvementStock->operation_sens = 'Entree';
            //             $mouvementStock->quantiteAvant = $stockInitial;
            //             $mouvementStock->quantite =  $receptionArticle->pivot->quantite;
            //             $mouvementStock->quantiteApres = $stockInitial + $receptionArticle->pivot->quantite;
            //             $mouvementStock->commentaires =  '';
            //             $mouvementStock->save();
            //             #
            //         }
            //     }
            // }
            // foreach ($articles as $receptionArticle) {
            //     \Log::info('Log: '.$receptionArticle[0]);
            // }
            // $commande->statutBC = 2;
            // $commande->save();
            // $commande->receptions()->save($reception);
            #################################"
            #Verifier si le chantier dispose d'un ou plusieurs dépots, au premier cas
            # procéder directement à l'entreposage de la reception
            #################################


                if ($commande->chantier->depots->count() > 1) {
                    $depot_unique = false;
                } else {
                    $depot_unique = true;
                    $depot = $commande->chantier->depots->first();
                }

                if ($articles) {
                    foreach ($articles as $article) {
                        // \Log::info('Aricles: ' . $articles);
                        $commande_item = $commande->commande_items
                            ->where('article_id', '=', $article['article_id'])
                            ->first();

                        if ($commande_item) {
                            $id_reception_item = DB::table('reception_items')->insertGetId(
                                [
                                    'reception_id' => $reception_id,
                                    'article_id' => $article['article_id'],
                                    'commande_item_id' => $commande_item['id'],
                                    'quantite_receptionnee' => $article['quantite_receptionnee'],
                                    'commentaires' => $article['commentaires'],
                                    'created_at' => Carbon::now(),
                                    'date_RC' => $reception->date_RC,
                                ]
                            );

                            if ($depot_unique) {
                                #Récuperer le stock
                                $stock = Stock::where('article_id', '=', $article['article_id'])
                                    ->where('depot_id', '=', $depot->id)
                                    ->first();
                                if ($stock) {
                                    # Mettre à jour le stock
                                    $stockInitial = $stock->quantiteDisponible;
                                    $stock->quantiteDisponible += $article['quantite_receptionnee'];
                                    $stock->save();
                                    # Ajouter l'historisation du Mouvement de stock
                                    // $mouvementStock = new MouvementStock();
                                    // $mouvementStock->stock_id =  $stock->id;
                                    // $mouvementStock->operation_id =  $id_reception_item;
                                    // $mouvementStock->operation_type = 'Reception';
                                    // $mouvementStock->operation_sens = 'Entree';
                                    // $mouvementStock->quantiteAvant = $stockInitial;
                                    // $mouvementStock->quantite =  $article['quantite_receptionnee'];
                                    // $mouvementStock->quantiteApres = $stockInitial + $article['quantite_receptionnee'];
                                    // $mouvementStock->commentaires =  '';
                                    // $mouvementStock->save();

                                    DB::table('mouvement_stocks')->insertGetId(
                                        [
                                            'stock_id' => $stock->id,
                                            'operation_id' => $id_reception_item,
                                            'operation_type' => 'Reception',
                                            'operation_sens' => 'Entrée',
                                            'quantiteAvant' => $stockInitial,
                                            'quantite' => $article['quantite_receptionnee'],
                                            'quantiteApres' => $stockInitial + $article['quantite_receptionnee'],
                                            'created_at' => Carbon::now(),
                                            'date_RC' => $reception->date_RC,
                                        ]
                                    );
                                    DB::table('receptions')
                                        ->where('id', '=', $reception_id)
                                        ->update(
                                            [
                                                'statut_RC' => 2
                                            ]
                                        );
                                }
                            }
                        } else {
                            throw new HttpException(404, 'Cet article n\'a pas été commandé');
                        }
                    }
                }

                DB::table('parametres')
                    ->Where('code_param', 'BR' . $commande->chantier->code_chantier)
                    ->update(['num_val' => $lastNumeroRC->num_val + 1]);

                $commande = Commande::findOrFail($request->commande_id);
                if ($commande->is_receptionne == false)
                    $commande->is_receptionne = true;
                $commande->save();

                if($request->typedereception == "Reception partielle finale")
                {
                    global $numcommande;
                    $commande = Commande::findOrFail($request->commande_id);
                    $numcommande = $commande->numero_DA;
                    $commande->isreceptionpartiellefinale = true;
                    $commande->save();

                    $user = User::first();

                    $details = [

                        'greeting' => 'Bonjour / Bonsoir M. '.$user->name,

                        'body' => 'Notification d\'une réception partielle finale sur le bon de commande : '. $commande->numero_BC,

                        'thanks' => 'Merci de vous connectez à la plateforme CHANTI-GEST pour traiter cette réception',

                        'actionText' => 'Accéder à CHANTI-GEST',

                        'actionURL' => url('http://10.100.35.2'),

                        'order_id' => 101

                    ];
                    Notification::send($user, new ReceptionNotification($details));

                }


                DB::commit();
                return new ReceptionResource($reception);
            } else {
                    $commandecanceled = Commande::findOrFail($request->command_id);
                    $commandecanceled->isCanceled = true;
                    $commandecanceled->canceledcommentaire = $request->canceledcommentaire;
                    $commandecanceled->save();
            }

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function show(Reception $reception)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function edit(Reception $reception)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reception $reception)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reception  $reception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reception $reception)
    {
        //
    }
}
