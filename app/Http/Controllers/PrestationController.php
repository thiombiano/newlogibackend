<?php

namespace App\Http\Controllers;

use App\Http\Resources\PrestationCollection;
use App\Models\Prestation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrestationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Prestation $prestation)
    {
        return PrestationCollection::collection($prestation->where('isCanceled', '=', false)->get()) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'prestataire_id' => 'required',
            ]);

            global $datefin;
            if($request->datefin != "")
                $datefin = date('Y-m-d', strtotime($request->datefin));
            else
                $datefin = null;
            $prestation = new Prestation();
            $prestation->prestataire_id = $request->prestataire_id;
            $prestation->maison_id = $request->maison_id;
            $prestation->date_Prestation = date('Y-m-d');
            $prestation->prefat_id = $request->prefat_id;
            $prestation->etape_construction_id = $request->etape_construction_id;
            $prestation->datedebut = date('Y-m-d',  strtotime($request->datedebut));
            $prestation->datefin = $datefin;
            $prestation->typecontrat = $request->typecontrat;
            $prestation->save();

            return response([
                'data' => $prestation
            ], Response::HTTP_CREATED);

        }   catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return PrestationCollection::collection(Prestation::where('id','=',$id)->where('isCanceled', '=', false)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }


    public function showprestationbymaison($idmaison) {
        try {
            return PrestationCollection::collection(Prestation::where('maison_id','=',$idmaison)->where('isCanceled', '=', false)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }


    public function showprestationbyprefat($idprefat) {
        try {
            return PrestationCollection::collection(Prestation::where('prefat_id','=',$idprefat)->where('isCanceled', '=', false)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function edit(Prestation $prestation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'prestataire_id' => 'required',
            ]);

            $prestation = Prestation::findOrFail($id);
            $prestation->prestataire_id = $request->prestataire_id;
            $prestation->maison_id = $request->maison_id;
            $prestation->prefat_id = $request->prefat_id;
            $prestation->etape_construction_id = $request->etape_construction_id;
            $prestation->datedebut = date('Y-m-d',  strtotime($request->datedebut));
            $prestation->datefin =  date('Y-m-d', strtotime($request->datefin));
            $prestation->typecontrat = $request->typecontrat;
            $prestation->save();

            return response([
                'data' => $prestation
            ], Response::HTTP_CREATED);

        }   catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $prestation = Prestation::findOrFail($id);
            $prestation->isCanceled = true;
            $prestation->save();
            return response([
                'data' =>  'Prestation supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
