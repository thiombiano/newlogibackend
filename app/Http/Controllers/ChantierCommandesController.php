<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\CommandeItem;
use App\Models\Chantier;
use Illuminate\Http\Request;
use App\Http\Resources\ChantierCommandesCollection;
use App\Http\Resources\CommandeResource;
use Illuminate\Support\Facades\DB;

class ChantierCommandesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chantier $chantier)
    {
        return   $chantier->commandes()->where('isCanceled','=', false)->orderBy('id', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request, Chantier $chantier)
    // {
    //     DB::beginTransaction();
    //     try {
    //         $commande = new Commande($request->all());
    //         //$commande->dateBC = \Carbon\Carbon::parse(date_format($commande->dateBC,'d/m/Y H:i:s'));
    //         $commande->date_BC = \Carbon\Carbon::createFromFormat('d/m/Y', $commande->date_BC);
    //         //$commande->dateLivraisonCible = \Carbon\Carbon::now()->addDays(7);
    //         $commande->date_lvraison_prevue = \Carbon\Carbon::createFromFormat('d/m/Y', $commande->date_lvraison_prevue);

    //         $commande->statut_BC = 1;
    //         // $chantier->commandes()->save($commande);

    //         //$chantier->commandes()->save($commande);

    //         $lastNumeroBC = DB::table('parametres')
    //             ->Where('code_param', 'BC' . $chantier->code_chantier)
    //             ->first();

    //         \Log::Info($lastNumeroBC->code_param . $lastNumeroBC->num_val);

    //         $commande_id = DB::table('commandes')->insertGetId(
    //             [
    //                 'numero_BC' => $lastNumeroBC->code_param . str_pad($lastNumeroBC->num_val, 5, '0', STR_PAD_LEFT),
    //                 'numero_DA' => $request->numero_DA,
    //                 'date_BC' =>  $commande->date_BC,
    //                 'chantier_id' => $chantier->id,
    //                 'statut_BC' => $commande->statut_BC,
    //                 'date_lvraison_prevue' => $commande->date_lvraison_prevue,
    //                 'fournisseur_id' => $request->fournisseur_id,
    //                 'is_prix_ttc' => $request->is_prix_ttc,
    //                 'montant_ttc' => $request->montant_ttc,
    //             ]
    //         );

    //         $articles = $request->articles;
    //         if ($articles) {
    //             foreach ($articles as $article) {
    //                 // \Log::info('Aricles: ' . $articles);
    //                 // $commande_item = new CommandeItem();
    //                 // $commande_item->article_id = $article['article_id'];
    //                 // $commande_item->commande_id = $commande->id;
    //                 // $commande_item->quantite_commandee = $article['quantite'];
    //                 // $commande_item->prix_unitaire = $article['prixUnitaire'];
    //                 // $commande_item->commentaires = $article['commentaires'];
    //                 // $commande->commande_items->push($commande_item);

    //                 DB::table('commande_items')->insert(
    //                     [
    //                         'commande_id' => $commande_id,
    //                         'article_id' => $article['article_id'],
    //                         'quantite_commandee' => $article['quantite'],
    //                         'prix_unitaire' => $article['prix_unitaire'],
    //                         'commentaires' => $article['commentaires']
    //                     ]
    //                 );
    //             }
    //         }

    //         DB::table('parametres')
    //             ->Where('code_param', 'BC' . $chantier->code_chantier)
    //             ->update(['num_val' => $lastNumeroBC->num_val + 1]);

    //         DB::commit();

    //         return  response(DB::table('commandes')->where('id', $commande_id)->get());
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         throw $e;
    //     }
    // }

    public function store(Request $request, Chantier $chantier)
    {
        DB::beginTransaction();
        try {
            $commande = new Commande($request->all());
            //$commande->dateBC = \Carbon\Carbon::parse(date_format($commande->dateBC,'d/m/Y H:i:s'));
            $commande->date_BC = date('Y-m-d', strtotime($commande->date_BC));
            //$commande->dateLivraisonCible = \Carbon\Carbon::now()->addDays(7);
            $commande->date_lvraison_prevue = date('Y-m-d', strtotime($commande->date_lvraison_prevue));
            $commande->personneacontacter = $request->personneacontacter;
            $commande->montant_ttc = $request->montant_ttc;
            $commande->statut_BC = 1;
            // $chantier->commandes()->save($commande);

            //$chantier->commandes()->save($commande);

            $lastNumeroBC = DB::table('parametres')
                ->Where('code_param', 'BC' . $chantier->code_chantier)
                ->first();

            \Log::Info($lastNumeroBC->code_param . $lastNumeroBC->num_val);

            $commande_id = DB::table('commandes')->insertGetId(
                [
                    'numero_BC' => $lastNumeroBC->code_param . str_pad($lastNumeroBC->num_val, 5, '0', STR_PAD_LEFT),
                    'numero_DA' => $request->numero_DA,
                    'date_BC' =>  $commande->date_BC,
                    'chantier_id' => $chantier->id,
                    'statut_BC' => $commande->statut_BC,
                    'date_lvraison_prevue' => $commande->date_lvraison_prevue,
                    'fournisseur_id' => $request->fournisseur_id,
                    'montant_ttc' => $request->montant_ttc,
                    'montant_tva' => $request->montant_tva,
                    'montant_bic' => $request->montant_bic,
                    'personneacontacter' => $request->personneacontacter,
                    'mode_paiement'=> $request->mode_paiement,
                    'modepaiment_id' => $request->modepaiment_id
                ]
            );

            $articles = $request->articles;
            if ($articles) {
                foreach ($articles as $article) {
                    // \Log::info('Aricles: ' . $articles);
                    // $commande_item = new CommandeItem();
                    // $commande_item->article_id = $article['article_id'];
                    // $commande_item->commande_id = $commande->id;
                    // $commande_item->quantite_commandee = $article['quantite'];
                    // $commande_item->prix_unitaire = $article['prixUnitaire'];
                    // $commande_item->commentaires = $article['commentaires'];
                    // $commande->commande_items->push($commande_item);

                    DB::table('commande_items')->insert(
                        [
                            'commande_id' => $commande_id,
                            'article_id' => $article['article_id'],
                            'quantite_commandee' => $article['quantite'],
                            'is_bic' => $article['is_bic'],
                            'is_tva' => $article['is_tva'],
                            'valueofbic' => $article['valueofbic'],
                            'prix_unitaire' => $article['prix_unitaire'],
                            'commentaires' => $article['commentaires']
                        ]
                    );
                }
            }


            DB::table('parametres')
                ->Where('code_param', 'BC' . $chantier->code_chantier)
                ->update(['num_val' => $lastNumeroBC->num_val + 1]);

            DB::commit();

            return  response(DB::table('commandes')->where('id', $commande_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function show(Chantier $chantier, Commande $commande)
    {
        return new CommandeResource($commande);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function update($commande_id, Request $request)
    {
        try {
            $commande =  Commande::findOrFail($commande_id);
            $commande->date_BC = date('Y-m-d', strtotime($request->date_BC));
            $commande->date_lvraison_prevue = date('Y-m-d', strtotime($request->date_lvraison_prevue));
            $commande->montant_ttc = $request->montant_ttc;
            $commande->montant_tva = $request->montant_tva;
            $commande->montant_bic = $request->montant_bic;
            $commande->numero_DA = $request->numero_DA;
            $commande->personneacontacter = $request->personneacontacter;
            $commande->mode_paiement = $request->mode_paiement;
            $commande->fournisseur_id = $request->fournisseur_id;
            $commande->modepaiment_id = $request->modepaiment_id;
            $commande->statut_BC = 1;
            $commande->save();

            DB::table('commande_items')->where('commande_id','=', $commande_id)->delete();

            $articles = $request->articles;
            if ($articles) {

                foreach ($articles as $article) {

                    $data = array(
                        'commande_id' => $commande_id,
                        'article_id' => $article['article_id'],
                        'quantite_commandee' => $article['quantite'],
                        'prix_unitaire' => $article['prix_unitaire'],
                        'commantaires'  => $article['commentaires'],
                        'is_bic' => $article['is_bic'],
                        'is_tva' => $article['is_tva'],
                        'valueofbic' => $article['valueofbic'],
                    );
                    CommandeItem::updateOrCreate(
                        [
                            'commande_id' => $commande_id,
                            'article_id' => $article['article_id'],
                        ], $data
                    );
                }
            }
            return  response(DB::table('commandes')->where('id', $commande_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }


    public function updateforbonannulation($commande_id, Request $request)
    {
        try {
            $commande =  Commande::findOrFail($commande_id);
            $commande->canceledcommentaire = $request->canceledcommentaire;
            $commande->isCanceled = true;
            $commande->save();
            return  response(DB::table('commandes')->where('id', $commande_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commande $commande)
    {
        //
    }



    public function save(Request $request, Chantier $chantier)
    {
        try {
            $commande_id = DB::table('commandes')->insertGetId(
                [
                    'numero_BC' => $request->numero_BC,
                    'date_BC' =>  $request->date_BC,
                    'fournisseur_id' => $request->fournisseur_id,
                    'date_lvraison_prevue' => $request->date_lvraison_prevue,
                    'commentaires' => $request['commentaires']
                ]
            );
            $articles = $request->articles;
            if ($articles) {
                foreach ($articles as $article) {
                    DB::table('commande_items')->insert(
                        [
                            'commande_id' => $commande_id,
                            'article_id' => $article['article_id'],
                            'quantite_commandee' => $article['quantite'],

                        ]
                    );
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
