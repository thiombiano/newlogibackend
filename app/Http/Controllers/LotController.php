<?php

namespace App\Http\Controllers;

use App\Http\Resources\LotCollection;
use App\Http\Resources\MaisonCollection;
use App\Models\Lot;
use App\Models\Maison;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class LotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Lot $lot)
    {
//        return   Lot::where('is_actif', '=', true)->get();
        return  LotCollection::collection( Lot::where('is_actif', '=', true)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function getlottoaddhome($idchantier) {
        try {
            return DB::select(DB::raw('select l.id, l.libellelot from sections s, lots l, chantiers c where s.id = l.section_id and c.id = s.chantier_id and l.is_actif = true  and s.chantier_id = "'.$idchantier.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libellelot' => 'required',
            ]);

            $lot = new Lot($request->all());
            $lot->section_id = $request->section_id;
            $lot->save();
            return response([
                'data' => $lot
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return  LotCollection::collection(Lot::where('id', '=', $id)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function showmaisonbysection($idsection)
    {
        try {
            return  LotCollection::collection(Lot::where('section_id', '=', $idsection)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function showmaisonbychantier($idchantier)
    {
        try {
            $maison = DB::select(DB::raw('select *  from sections s, lots l, chantiers c, maisons m where s.id = l.section_id and c.id = s.chantier_id and  m.lot_id = l.id and l.is_actif = true  and s.chantier_id = "'.$idchantier.'" '));
            return MaisonCollection::collection(collect($maison)) ;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        try {
            $request->validate([
                'libellelot' => 'required',
            ]);
            $lot = Lot::findOrFail($id);
            $lot->libellelot = $request->libellelot;
            $lot->section_id = $request->section_id;
            $lot->save();
            return response([
                'data' =>  $lot
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        $lot = Lot::findOrFail($id);
        if ($lot)
            $lot->is_actif = false;
        $lot->save();
        return response(['data' => $lot], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
