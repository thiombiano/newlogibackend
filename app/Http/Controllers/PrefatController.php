<?php

namespace App\Http\Controllers;

use App\Models\Chantier;
use App\Models\Prefat;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PrefatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chantier $chantier)
    {
        return   $chantier->prefats()->with('technicien', 'magasinier')->where('is_actif','=', true)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function showprefatbyid($id)
    {
        try {
            $maison =  DB::select(DB::raw('select p.libelleprefat from prefats p  where  p.id = "'.$id.'" '));
            return collect($maison) ;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libelleprefat' => 'required',
            ]);

            $prefat = new Prefat($request->all());
            $prefat->chantier_id = $request->chantier_id;
            $prefat->technicien_id = $request->technicien_id;
            $prefat->magasinier_id = $request->magasinier_id;
            $prefat->save();
            return response([
                'data' => $prefat
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Prefat  $prefat
     * @return \Illuminate\Http\Response
     */
    public function show(Prefat $prefat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Prefat  $prefat
     * @return \Illuminate\Http\Response
     */
    public function edit(Prefat $prefat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Prefat  $prefat
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        try {
            $request->validate([
                'libelleprefat' => 'required',
            ]);
            $prefat = Prefat::findOrFail($id);
            $prefat->libelleprefat = $request->libelleprefat;
            $prefat->chantier_id = $request->chantier_id;
            $prefat->technicien_id = $request->technicien_id;
            $prefat->magasinier_id = $request->magasinier_id;
            $prefat->save();
            return response([
                'data' =>  $prefat
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Prefat  $prefat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prefat = Prefat::findOrFail($id);
        if ($prefat)
            $prefat->is_actif = false;
        $prefat->save();

        return response(['data' => $prefat], Response::HTTP_OK);
    }
}
