<?php

namespace App\Http\Controllers;

use App\CommandeArticles;
use Illuminate\Http\Request;

class CommandeArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CommandeArticles  $commandeArticles
     * @return \Illuminate\Http\Response
     */
    public function show(CommandeArticles $commandeArticles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CommandeArticles  $commandeArticles
     * @return \Illuminate\Http\Response
     */
    public function edit(CommandeArticles $commandeArticles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CommandeArticles  $commandeArticles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommandeArticles $commandeArticles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CommandeArticles  $commandeArticles
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommandeArticles $commandeArticles)
    {
        //
    }
}
