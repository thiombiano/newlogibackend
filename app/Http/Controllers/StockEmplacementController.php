<?php

namespace App\Http\Controllers;

use App\Http\Resources\StockEmplacementCollection;
use App\Models\Article;
use App\Models\Stock;
use App\Models\StockEmplacement;
use Illuminate\Http\Request;

class StockEmplacementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StockEmplacementCollection::collection(StockEmplacement::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Log::info('Entrée');
        $articles = Article::all();
        $emplacementId =  $request->emplacement_id;
        //Initialiser le stock

        foreach ($articles as $article) {
            \Log::info($article->id);
            $stockemplacement = StockEmplacement::create([
                'emplacement_id' => $emplacementId,
                'article_id'  => $article->id,
                'stockMinimal' => 0,
                'quantiteDisponible' => 0,
                'commentaires' => ''
            ]);
            \Log::info($stockemplacement->id);
        }

        return  'Initialisation stocks emplacements : OK';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StockEmplacement  $stockEmplacement
     * @return \Illuminate\Http\Response
     */
    public function show(StockEmplacement $stockEmplacement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StockEmplacement  $stockEmplacement
     * @return \Illuminate\Http\Response
     */
    public function edit(StockEmplacement $stockEmplacement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StockEmplacement  $stockEmplacement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockEmplacement $stockEmplacement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StockEmplacement  $stockEmplacement
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockEmplacement $stockEmplacement)
    {
        //
    }
}
