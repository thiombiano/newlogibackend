<?php

namespace App\Http\Controllers;

use App\Models\Employe;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class EmployeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employe $employe)
    {
        return $employe->with('fonction')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'codeEmploye' => 'required',
            ]);
            $employe = new Employe();
            $employe->codeEmploye = $request->codeEmploye;
            $employe->prenom = $request->prenom;
            $employe->nom = $request->nom;
            $employe->tel = $request->tel;
            $employe->email = $request->email;
            $employe->cnib = $request->cnib;
            $employe->fonction_id = $request->fonction_id;
            $employe->chantier_id = $request->chantier_id;
            $employe->societe_constructive_id = $request->societe_constructive_id;
            $employe->save();

        }  catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function show(Employe $employe)
    {
        return  $employe;
    }

    public function showemployebyfonction($idfonction) {
     try {
           $employe=  DB::select(DB::raw('select e.id, e.prenom, e.nom from employes e, fonctions f where e.fonction_id = f.id and f.id = "'.$idfonction.'"'));
               return collect($employe) ;
          } catch (\Exception $e) {
            return response([
                    'Erreur' => $e
                ], Response::HTTP_NOT_FOUND);
         }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function edit(Employe $employe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employe $employe)
    {
        try {

            $request->validate([
                'codeEmploye' => 'required'
            ]);
            $employe->codeEmploye = $request->codeEmploye;
            $employe->prenom = $request->prenom;
            $employe->nom = $request->nom;
            $employe->tel = $request->tel;
            $employe->email = $request->email;
            $employe->cnib = $request->cnib;
            $employe->fonction_id = $request->fonction_id;
            $employe->chantier_id = $request->chantier_id;
//            if($request->societe_constructive_id)
                $employe->societe_constructive_id = $request->societe_constructive_id;
//            else
//                $employe->societe_constructive_id = 1;
            $employe->save();
        }  catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employe $employe)
    {
        //
    }
}
