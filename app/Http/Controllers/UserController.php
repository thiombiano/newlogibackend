<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserCollection;
use App\Models\Role;
use App\Models\RolesHasPermission;
use App\Models\UsersHasRoles;
use App\User;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return UserCollection
     */
    public function index(User $user)
    {
        return  UserCollection::collection($user->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $request->validate([
                'login' => 'required',
                'password' => 'required'
            ]);
            $user = User::create([
                'name' => $request->name,
                'codeemploye' => $request->codeemploye,
                'prenomemploye' => $request->prenomemploye,
                'telemploye' => $request->telemploye,
                'login' => $request->login,
                'email' => $request->email,
                'password' => $request->password,
                'realpassword' => $request->password,
                'isActif' => $request->isActif
            ]);
            $lastIdUserInserted = $user->id;
            $image = $request->file('avatar');
            if(isset($image))
            {

                $image = $request->file('avatar');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save( public_path('img/profile/' . $filename ) );
                $chemin='profile/';
                $user->avatar = $chemin.$filename;
                $user->save();
            }
            else
            {
                $filename = 'defaultUser.png';
                $chemin='profile/';
                $user->avatar = $chemin.$filename;
                $user->save();
            }

            foreach (json_decode($request->roles) as $p) {
                $userhasrole = new UsersHasRoles();
                $userhasrole->role_id = $p;
                $userhasrole->user_id = $lastIdUserInserted;
                $userhasrole->save();
            }
            return response([
                'data' => $user
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($id)
    {
        try {
            return UserCollection::collection(User::where('id','=',$id)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getusertechnicien() {
        try {
            return DB::select(DB::raw('select us.id, us.name, us.prenomemploye from users us , roles r, users_has_roles u where us.id= u.user_id and r.id=u.role_id and r.libellerole="TECHNICIEN"'));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }


    public function getusermagasinier() {
        try {
            return DB::select(DB::raw('select us.id, us.name, us.prenomemploye from users us , roles r, users_has_roles u where us.id= u.user_id and r.id=u.role_id and r.libellerole="MAGASINIER"'));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getusercontroleur() {
        try {
            return DB::select(DB::raw('select us.id, us.name, us.prenomemploye from users us , roles r, users_has_roles u where us.id= u.user_id and r.id=u.role_id and r.libellerole="CONTROLEUR"'));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        try {
            $request->validate([
                'login' => 'required'
            ]);

            global $isPasswordChange ;
            $imageCourantBD = $user->avatar;

            global $filename;

            if($request->avatar != null && $request->file('avatar'))
            {
                $image = $request->file('avatar');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                Image::make($image)->save( public_path('img/profile/' . $filename ) );

            }
            else
                $filename='';

            $chemin='profile/';
            $imageCourantAPP = $chemin.''.$filename;

            if($request->password == '')
                $isPasswordChange= false;
            else
                $isPasswordChange=true;



            $user->name = $request->name;
            $user->prenomemploye = $request->prenomemploye;
            $user->telemploye = $request->telemploye;
            $user->codeemploye = $request->codeemploye;
            $user->email = $request->email;
            $user->isActif = $request->isActif;
            $user->login = $request->login;
            $user->avatar = ($filename === '' ? $imageCourantBD : $imageCourantAPP);
            if($isPasswordChange==true)
            {
                $user->password = $request->password;
                $user->realpassword = $request->password;
            }
            $user->save();
            $iduser = $user->id;

            DB::table('users_has_roles')->where('user_id','=',$iduser)->delete();
            foreach (json_decode($request->roles)  as $p ){
                $userhasrole = new UsersHasRoles();
                $userhasrole->role_id = $p->id;
                $userhasrole->user_id = $iduser;
                $userhasrole->save();
            }
            return response([
                'data' =>  UserCollection::collection($user->where('id','=', $iduser)->get())
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
