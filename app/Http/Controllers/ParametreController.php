<?php

namespace App\Http\Controllers;

use App\Models\parametre;
use Illuminate\Http\Request;

class ParametreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function show(parametre $parametre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function edit(parametre $parametre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, parametre $parametre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\parametre  $parametre
     * @return \Illuminate\Http\Response
     */
    public function destroy(parametre $parametre)
    {
        //
    }
}
