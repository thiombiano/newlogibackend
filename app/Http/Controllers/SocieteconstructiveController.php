<?php

namespace App\Http\Controllers;

use App\Http\Resources\SocieteCollection;
use App\Http\Resources\SocieteResource;
use App\Models\SocieteConstructive;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SocieteconstructiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return SocieteConstructive[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return  SocieteCollection::collection( SocieteConstructive::all()) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libellesociete' => 'required',
            ]);

            $societeconstructive = new SocieteConstructive($request->all());
            $societeconstructive->save();
            return response([
                'data' => $societeconstructive
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SocieteConstructive  $societeConstructive
     * @return SocieteConstructive
     */
    public function show($id)
    {
        try {
            $societeConstructive = SocieteConstructive::findOrFail($id);
            return $societeConstructive;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SocieteConstructive  $societeConstructive
     * @return \Illuminate\Http\Response
     */
    public function edit(SocieteConstructive $societeConstructive)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SocieteConstructive  $societeConstructive
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'libellesociete' => 'required',
            ]);
            $societeConstructive = SocieteConstructive::findOrFail($id);
            $societeConstructive->libellesociete = $request->libellesociete;
            $societeConstructive->description = $request->description;
            $societeConstructive->save();
            return response([
                'data' =>  $societeConstructive
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SocieteConstructive  $societeConstructive
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $societeConstructive = SocieteConstructive::findOrFail($id);
            $societeConstructive->delete();
            return response([
                'data' =>  'Société supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
