<?php

namespace App\Http\Controllers;

use App\Http\Resources\TypemaisonCollection;
use App\Models\Role;
use App\Models\RolesHasPermission;
use App\Models\TypeMaison;
use App\Models\TypeMaisonHasEtapeConstruction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class TypemaisonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(TypeMaison $typeMaison)
    {
        return TypemaisonCollection::collection( $typeMaison->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libelletypemaison' => 'required',
            ]);
            $typemaison = new TypeMaison();
            $typemaison->libelletypemaison = $request->libelletypemaison;
            $typemaison->description = $request->description;
            $typemaison->save();
            $idtypemaison = $typemaison->id;

            foreach ($request->etapes  as $p ){
                $typehasetape = new TypeMaisonHasEtapeConstruction();
                $typehasetape->type_maison_id = $idtypemaison;
                $typehasetape->etape_construction_id = $p;
                $typehasetape->save();
            }

            return response([
                'data' => $typemaison
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypeMaison  $typeMaison
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($id)
    {
        try {
            return TypemaisonCollection::collection(TypeMaison::where('id','=',$id)->get());
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypeMaison  $typeMaison
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeMaison $typeMaison)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeMaison  $typeMaison
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'libelletypemaison' => 'required',
            ]);
            $typeMaison = TypeMaison::findOrFail($id);
            $typeMaison->libelletypemaison = $request->libelletypemaison;
            $typeMaison->description = $request->description;
            $typeMaison->save();
            $idtypemaison = $typeMaison->id;

            DB::table('type_has_etapes')->where('type_maison_id','=',$idtypemaison)->delete();
            foreach ($request->etapes  as $p ){
                $typeshasetapes = new TypeMaisonHasEtapeConstruction();
                $typeshasetapes->type_maison_id = $idtypemaison;
                $typeshasetapes->etape_construction_id = $p['id'];
                $typeshasetapes->save();
            }
            return response([
                'data' =>  $typeshasetapes
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeMaison  $typeMaison
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $typeMaison = TypeMaison::findOrFail($id);
            $typeMaison->delete();
            return response([
                'data' =>  'Type maison supprimée avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
