<?php

namespace App\Http\Controllers;

use App\Http\Resources\BesoinJournalierResource;
use App\Models\BesoinJitem;
use App\Models\BesoinJournalier;
use App\Models\Chantier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use function foo\func;

class BesoinJournalierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chantier $chantier)
    {
        return BesoinJournalierResource::collection($chantier->besoinJournaliers()->orderBy('id', 'desc')->get());
    }

    public function indexbesionsbyutilisateur($idchantier, $idutilisateur) {
        $chantier = Chantier::findOrFail($idchantier);
        return BesoinJournalierResource::collection($chantier->besoinJournaliers()->where('technicien_id','=', $idutilisateur)->orderBy('id', 'desc')->get());
    }

    public function showtodatbesoinjournalier($id)
    {
        $today = Carbon::today();
        $yesterday = $today->subDay();
        $now = Carbon::now();
        $chantier = Chantier::findOrFail($id);
        return BesoinJournalierResource::collection( $chantier->besoinJournaliers()->where(DB::raw('DATE(dateBJ)'), '=', $yesterday)
            ->orWhere(function($query) {
                $today = Carbon::today();
                $query->where(DB::raw('DATE(dateBJ)'), '=', $today->format('Y-m-d'));
            })->orderBy('id', 'DESC')->get());
    }


    public function getalldetailbesoinlevelone($idbesoinjournalier) {
        try {
            return DB::select(DB::raw('SELECT b.id AS idbesoinjournalier, m.id AS idmaison, b.dateBJ, m.libellemaison, b.cahierjournalier, f.libelleFonction, e.libelleetape
                                              FROM besoin_journaliers b, besoin_jitems bj, maisons m, fonctions f, etape_constructions e
                                              WHERE b.id = bj.besoin_journalier_id AND m.id = bj.maison_id AND f.id = bj.fonction_id
                                              AND e.id = bj.etape_construction_id   AND b.id = "'.$idbesoinjournalier.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }




    public function getalldetailbesoininprefatlevelone($idbesoinjournalier) {
        try {
            return DB::select(DB::raw('SELECT b.id AS idbesoinjournalier, pre.id AS idprefat, b.dateBJ, pre.libelleprefat, b.cahierjournalier, f.libelleFonction
                                              FROM besoin_journaliers b, besoin_jitems bj, prefats pre, fonctions f
                                              WHERE b.id = bj.besoin_journalier_id AND pre.id = bj.prefat_id AND f.id = bj.fonction_id
                                              AND b.id = "'.$idbesoinjournalier.'" '));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }



    public function getalldetailbesoinbymaisonleveltwo($idbesoinjournalier, $idmaison) {
        try {
            return DB::select(DB::raw('SELECT a.libelle_article, bj.quantiteBesoin FROM besoin_journaliers b, besoin_jitems bj, maisons m, fonctions f, etape_constructions e, articles a WHERE b.id = bj.besoin_journalier_id AND m.id = bj.maison_id AND f.id = bj.fonction_id AND a.id = bj.article_id AND e.id = bj.etape_construction_id AND b.id = "'.$idbesoinjournalier.'" AND  m.id = "'.$idmaison.'"'));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getalldetailbesoinbyprefatleveltwo($idbesoinjournalier, $idprefat) {
        try {
            return DB::select(DB::raw('SELECT a.libelle_article, bj.quantiteBesoin FROM besoin_journaliers b, besoin_jitems bj, prefats pre, fonctions f, articles a WHERE b.id = bj.besoin_journalier_id AND pre.id = bj.prefat_id AND f.id = bj.fonction_id AND a.id = bj.article_id  AND b.id = "'.$idbesoinjournalier.'" AND  pre.id = "'.$idprefat.'"'));
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function touslesbesoinsjournaliersitems()
    {
        $today = Carbon::today();
        $now = Carbon::now();
        return BesoinJournalierResource::collection( BesoinJournalier::where(DB::raw('DATE(dateBJ)'), '=', $today->format('Y-m-d'))->get());;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Chantier $chantier)
    {
        DB::beginTransaction();
        try {
            $besoinjournalier = new BesoinJournalier($request->all());
            $besoinjournalier->journee_id = $request->journee_id;
            $besoinjournalier->chantier_id = $request->chantier_id;
            $besoinjournalier->technicien_id = $request->technicien_id;
            $besoinjournalier->typebesoin = $request->typebesoin;
            $besoinjournalier->dateBJ = date('d-m-Y H:i:s');
            $lastcahierjournalier = DB::table('parametres')
                ->Where('code_param', 'CAJ' . $chantier->code_chantier)
                ->first();

            $besoinjournalier_id = DB::table('besoin_journaliers')->insertGetId(
                [
                    'cahierjournalier' => $lastcahierjournalier->code_param . str_pad($lastcahierjournalier->num_val, 5, '0', STR_PAD_LEFT),
                    'journee_id' => $besoinjournalier->journee_id,
                    'technicien_id' => $besoinjournalier->technicien_id,
                    'dateBJ' => date('Y-m-d H:i:s'),
                    'typebesoin' =>    $besoinjournalier->typebesoin,
                    'chantier_id' => $chantier->id
                ]
            );

            $articles = $request->articles;
            if ($articles) {
                foreach ($articles as $article) {
                    DB::table('besoin_jitems')->insert(
                        [
                            'besoin_journalier_id' => $besoinjournalier_id,
                            'article_id' => $article['article_id'],
                            'maison_id' => $article['maison_id'],
                            'prefat_id' => $article['prefat_id'],
                            'fonction_id' => $article['fonction_id'],
                            'etape_construction_id' => $article['etape_construction_id'],
                            'quantiteBesoin' => $article['quantiteBesoin'],
                            'commentaires' => $article['commentaires']
                        ]
                    );
                }
            }

            DB::table('parametres')
                ->Where('code_param', 'CAJ' . $chantier->code_chantier)
                ->update(['num_val' => $lastcahierjournalier->num_val + 1]);


            DB::commit();
            return response([
                'data' => new BesoinJournalierResource($besoinjournalier)
            ], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BesoinJournalier  $besoinJournalier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $besoin = BesoinJournalier::findOrFail($id);
        return new BesoinJournalierResource($besoin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BesoinJournalier  $besoinJournalier
     * @return \Illuminate\Http\Response
     */
    public function edit(BesoinJournalier $besoinJournalier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BesoinJournalier  $besoinJournalier
     * @return \Illuminate\Http\Response
     */
    public function update($besoin_id, Request $request)
    {
        try {
            $besoin_journalier = BesoinJournalier::findOrFail($besoin_id);
            $besoin_journalier->journee_id = $request->journee_id;
            $besoin_journalier->chantier_id = $request->chantier_id;
            $besoin_journalier->technicien_id = $request->technicien_id;
            $besoin_journalier->typebesoin = $request->typebesoin;
            $besoin_journalier->save();
            DB::table('besoin_jitems')->where('besoin_journalier_id','=', $besoin_id)->delete();
            $articles = $request->articles;
            if ($articles) {

                foreach ($articles as $article) {

                    $data = array(
                        'besoin_journalier_id' => $besoin_id,
                        'article_id' => $article['article_id'],
                        'maison_id' => $article['maison_id'],
                        'prefat_id' => $article['prefat_id'],
                        'fonction_id' => $article['fonction_id'],
                        'etape_construction_id' => $article['etape_construction_id'],
                        'quantiteBesoin' => $article['quantiteBesoin'],
                        'commentaires' => $article['commentaires'],
                    );
                    BesoinJitem::updateOrCreate(
                        [
                            'besoin_journalier_id' => $besoin_id,
                            'article_id' => $article['article_id'],
                            'maison_id' => $article['maison_id'],
                            'prefat_id' => $article['prefat_id'],
                            'fonction_id' => $article['fonction_id'],
                            'etape_construction_id' => $article['etape_construction_id']
                        ], $data
                    );
                }
            }
            return  response(DB::table('besoin_journaliers')->where('id', $besoin_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function updateforbesoinannulation($besoin_id, Request $request)
    {
        try {
            $besoin=  BesoinJournalier::findOrFail($besoin_id);
            $besoin->canceledcommentaire = $request->canceledcommentaire;
            $besoin->isCanceled = true;
            $besoin->save();
            return  response(DB::table('besoin_journaliers')->where('id','=', $besoin_id)->get());
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BesoinJournalier  $besoinJournalier
     * @return \Illuminate\Http\Response
     */
    public function destroy(BesoinJournalier $besoinJournalier)
    {
        //
    }
}
