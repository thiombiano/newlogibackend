<?php

namespace App\Http\Controllers;

use App\Models\EtapeConstruction;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class EtapeconstructionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EtapeConstruction $etapeConstruction)
    {
        return $etapeConstruction->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'libelleetape' => 'required',
            ]);

            $etapeconstruction= new EtapeConstruction();
            $etapeconstruction->libelleetape = $request->libelleetape;
            $etapeconstruction->description = $request->description;
            $etapeconstruction->montant_brut = $request->montant_brut;
            $etapeconstruction->montant_macon = $request->montant_macon;
            $etapeconstruction->montant_verse_entreprise = $request->montant_verse_entreprise;
            $etapeconstruction->save();
            return response([
                'data' => $etapeconstruction
            ], Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EtapeConstruction  $etapeConstruction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $etapeconstruction = EtapeConstruction::findOrFail($id);
            return $etapeconstruction;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getetapebymaison($idmaison) {
        try {
            $maison =  DB::select(DB::raw('select et.id, et.libelleetape  from maisons m, etape_state_in_maisons esim, etape_constructions et where  m.id=esim.maison_id AND et.id = esim.etape_construction_id AND esim.etapeState = 0 AND  m.id = "'.$idmaison.'" '));
            return collect($maison) ;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function getetapebytypemaison($idtypemaison) {
        try {
            $maison =  DB::select(DB::raw('SELECT et.id, et.libelleetape from  type_maisons ty, etape_constructions et, type_has_etapes ha where ha.type_maison_id = ty.id and ha.etape_construction_id = et.id  and ty.id  = "'.$idtypemaison.'" '));
            return collect($maison) ;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }




    public function getetapedisponibleformaison($idmaison) {
        try {
            $etapedispo =  DB::select(DB::raw('SELECT ec.id, ec.libelleetape FROM  etape_constructions ec WHERE ec.id NOT IN (SELECT ec.id FROM etape_constructions ec, etape_state_in_maisons esim, maisons m WHERE  ec.id = esim.etape_construction_id AND m.id = esim.maison_id AND m.id = "'.$idmaison.'" )' ));
            return collect($etapedispo) ;
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_NOT_FOUND);
        }
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EtapeConstruction  $etapeConstruction
     * @return \Illuminate\Http\Response
     */
    public function edit(EtapeConstruction $etapeConstruction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EtapeConstruction  $etapeConstruction
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        try {
            $request->validate([
                'libelleetape' => 'required',
            ]);
            $etapeConstruction = EtapeConstruction::findOrFail($id);
            $etapeConstruction->libelleetape = $request->libelleetape;
            $etapeConstruction->description = $request->description;
            $etapeConstruction->montant_brut = $request->montant_brut;
            $etapeConstruction->montant_macon = $request->montant_macon;
            $etapeConstruction->montant_verse_entreprise = $request->montant_verse_entreprise;
            $etapeConstruction->save();
            return response([
                'data' =>  $etapeConstruction
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EtapeConstruction  $etapeConstruction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $etapeConstruction = EtapeConstruction::findOrFail($id);
            $etapeConstruction->delete();
            return response([
                'data' =>  'Société supprimé avec succes'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response([
                'Erreur' => $e
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
