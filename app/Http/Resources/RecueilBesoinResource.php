<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RecueilBesoinResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'numeroRB' => $this->numeroRB,
            'destinations' => $this->destinations,
            'chantier' => $this->chantier,
            'dateRB' => $this->dateRB,
            'statutRB' => $this->statutRB,
            'dateLivraisonCible' => $this->dateLivraisonCible,
            'isValidate' => $this->isValidate,
            'articles' => RecueilItemCollection::collection($this->articles)
        ];
    }
}
