<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChantierCommandesCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'numero_BC' => $this->numero_BC,
            'numero_DA' => $this->numero_DA,
            'date_BC' => $this->date_BC,
            'statut_BC' => $this->statut_BC,
            'date_lvraison_prevue' => $this->date_lvraison_prevue,
            'chantier_id' => $this->chantier_id,
            'chantier' => $this->chantier->code_chantier,
            'fournisseur_id' => $this->fournisseur_id,
            'fournisseur' => $this->fournisseur->raison_social,
            'isreceptionpartiellefinale' => $this->isreceptionpartiellefinale,
            'is_prix_ttc' => $this->is_prix_ttc,
//            'is_bic' => $this->is_bic,
//            'valueofbic' => $this->valueofbic,
            'montant_ttc' => $this->montant_ttc,
            'montant_tva' => $this->montant_tva,
            'montant_bic' => $this->montant_bic,

        ];
    }
}
