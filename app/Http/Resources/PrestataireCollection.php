<?php

namespace App\Http\Resources;

use App\Models\Fonction;
use App\Models\SocieteConstructive;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PrestataireCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'codeEmploye' => $this->codeEmploye,
            'prenom' => $this->prenom,
            'nom' => $this->nom,
            'tel' => $this->tel,
            'email' => $this->email,
            'cnib' => $this->cnib,
            'fonction_id' => $this->fonction_id,
            'societe_constructive_id' => $this->societe_constructive_id,
            'fonction' => FonctionCollection::collection(Fonction::where('id', '=', $this->fonction_id)->get()),
            'societe' => SocieteCollection::collection(SocieteConstructive::where('id', '=',  $this->societe_constructive_id)->get())
        ];
    }
}
