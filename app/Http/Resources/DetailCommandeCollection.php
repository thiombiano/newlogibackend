<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailCommandeCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'article_id' => $this->pivot->article_id,
            'quantite' => $this->pivot->quantite,
            //'quantiteLivree' => $this->commande->receptions()->sum('articles.quantite'),
            'prixUnitaire' => $this->pivot->prixUnitaire,
            'commentaires' => $this->pivot->commentaires,
            'article' => $this->libelleArticle,
            'is_bic' => $this->isBic,
            'valueofbic' => $this->valueofbic
        ];
    }
}
