<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'libelle_article' => $this->libelle_article,
            'reference' => $this->reference,
            'description' => $this->description,
            'prix_referent' => $this->prix_referent,
            'unite' => $this->unite_mesure->code,
            'is_bic' => $this->is_bic,
            'is_tva' => $this->is_tva,
            'valueofbic' => $this->valueofbic,
        ];
    }
}
