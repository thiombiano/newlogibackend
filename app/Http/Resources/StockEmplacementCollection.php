<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StockEmplacementCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'emplacement' => $this->emplacement->nomEmplacement,
            'emplacement_id' => $this->emplacement_id,
            'reference' => $this->article->reference,
            'article' => $this->article->libelle_article,
            'article_id' => $this->article_id,
            'quantiteDisponible' => $this->quantiteDisponible,
            'stockMinimal' => $this->stockMinimal,
            'commentaires' => $this->commentaires,
        ];
    }
}
