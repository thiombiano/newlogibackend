<?php

namespace App\Http\Resources;

use App\Models\Employe;
use App\Models\EnlevementItem;
use App\Models\EtapeConstruction;
use App\Models\Fonction;
use App\Models\Maison;
use App\Models\Prefat;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class BesoinItemJournalierCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $quantiteSortie =   $quantiteSortie = DB::select(DB::raw('select sum(quantiteASortir) AS quantiteASortir from enlevement_items e where e.besoin_jitems_id = "'. $this->pivot->id .'"'));
            return [
                'id' => $this->id,
                'reference' => $this->reference,
                'unite' => $this->unite_mesure->code,
                'article' => $this->libelle_article,
                'article_id' => $this->id,
                'quantiteBesoin' => $this->pivot->quantiteBesoin,
                'quantiteASortir' => $quantiteSortie[0]->quantiteASortir,
                'is_out' => $this->pivot->is_out,
                'reliquat' => $this->pivot->quantiteBesoin - $quantiteSortie[0]->quantiteASortir,
                'maison_id' => $this->pivot->maison_id,
                'prefat_id' => $this->pivot->prefat_id,
                'fonction_id' => $this->pivot->fonction_id,
                'etape_construction_id' => $this->pivot->etape_construction_id,
                'besoin_jitems_id' => $this->pivot->id,
                'commentaires'=> $this->pivot->commentaires,
                'nomprestataire' => Employe::select('nom')->where('fonction_id', '=', $this->pivot->fonction_id )->pluck('nom')->first(),
                'prenomprestataire' => Employe::select('prenom')->where('fonction_id', '=', $this->pivot->fonction_id )->pluck('prenom')->first(),
                'libelleetape' => EtapeConstruction::select('libelleetape')->where('id', '=', $this->pivot->etape_construction_id)->pluck('libelleetape')->first(),
                'libellemaison' => Maison::select('libellemaison')->where('id', '=', $this->pivot->maison_id)->pluck('libellemaison')->first(),
                'libelleprefat' => Prefat::select('libelleprefat')->where('id', '=',  $this->pivot->prefat_id)->pluck('libelleprefat')->first(),
                'libellefonction' => Fonction::select('libelleFonction')->where('id', '=', $this->pivot->fonction_id)->pluck('libelleFonction')->first()
            ];

    }
}
