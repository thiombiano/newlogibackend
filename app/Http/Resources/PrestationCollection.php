<?php

namespace App\Http\Resources;

use App\Http\Controllers\MaisonController;
use App\Models\Employe;
use App\Models\EtapeConstruction;
use App\Models\Maison;
use App\Models\Prefat;
use App\Models\Prestation;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PrestationCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'prestataire_id' => $this->prestataire_id,
            'maison_id' => $this->maison_id,
            'prefat_id' => $this->prefat_id,
            'etape_construction_id' => $this->etape_construction_id,
            'datedebut' => $this->datedebut,
            'datefin' => $this->datefin,
            'typecontrat' => $this->typecontrat,
            'etape' => EtapeConstructionCollection::collection(EtapeConstruction::where('id', '=',  $this->etape_construction_id)->get()),
            'prestataire' => PrestataireCollection::collection(Employe::where('id', '=', $this->prestataire_id)->get()),
            'maison' => MaisonCollection::collection(Maison::where('id', '=', $this->maison_id)->get()),
            'prefat' => PrefatCollection::collection(Prefat::where('id', '=', $this->pref_id)->get())

        ];
    }
}
