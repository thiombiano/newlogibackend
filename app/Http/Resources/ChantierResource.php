<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChantierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'codeChantier' => $this->codeChantier,
            'denomination' => $this->denomination,
            'description' => $this->description,
            'adresse' => $this->adresse,
        ];
    }
}
