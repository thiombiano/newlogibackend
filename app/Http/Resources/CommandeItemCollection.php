<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommandeItemCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->article->reference,
            'unite' => $this->article->unite_mesure->code,
            'article' => $this->article->libelle_article,
            'article_id' => $this->article->id,
            'quantite' => $this->quantite_commandee,
            'prix_unitaire' => $this->prix_unitaire,
            'prix_referent' => $this->article->prix_referent,
            'is_bic' => $this->is_bic,
            'is_tva' => $this->is_tva,
            'valueofbic' => $this->valueofbic,
            'quantite_receptionnee' => $this->reception_items->sum('quantite_receptionnee'),
            'reliquat' => $this->quantite_commandee - $this->reception_items->sum('quantite_receptionnee'),
            'commentaires' => $this->commentaires,
        ];
    }
}
