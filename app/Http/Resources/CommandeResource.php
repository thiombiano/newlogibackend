<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommandeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        global  $libmodepaiement, $modepaiment_id;
        if($this->modepaiment)
        {
            $libmodepaiement = $this->modepaiment->libellemodepaiement;
            $modepaiment_id = $this->modepaiment->id;
        }
        else
        {
            $libmodepaiement = '';
            $modepaiment_id='';
        }

        return [
            'id' => $this->id,
            'numero_BC' => $this->numero_BC,
            'numero_DA' => $this->numero_DA,
            'chantier' => $this->chantier->denomination,
            'chantier_id' => $this->chantier->id,
            'date_BC' => $this->date_BC,
            'fournisseur' => $this->fournisseur->raison_social,
            'fournisseur_id' => $this->fournisseur->id,
            'adresse_fournisseur' => $this->fournisseur->adresse,
            'telephone_fournisseur' => $this->fournisseur->tel,
            'is_prix_ttc' => $this->is_prix_ttc,
            'montant_ttc' => $this->montant_ttc,
            'montant_tva' => $this->montant_tva,
            'montant_bic' => $this->montant_bic,
            'date_lvraison_prevue' => $this->date_lvraison_prevue,
            'adresse_chantier' => $this->chantier->adresse,
            'statut_BC' => $this->statutBC,
            'mode_paiement' => $this->mode_paiement,
            'libellemodepaiement' => $libmodepaiement,
            'modepaiment_id' => $modepaiment_id,
            'personneacontacter' =>$this->personneacontacter,
            'isreceptionpartiellefinale' => $this->isreceptionpartiellefinale,
//            'is_bic' => $this->is_bic,
//            'valueofbic' => $this->valueofbic,
            'articles'  => CommandeItemCollection::collection($this->commande_items),
            'receptions'  => ReceptionCollection::collection($this->receptions),
        ];
    }
}
