<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MouvementStockCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'date' => $this->date_RC,
            'nature' => $this->operation_type,
            'numero_piece' => 'XXXXX',
            'entree' => $this->operation_sens == 'Entrée' ? $this->quantite : 0,
            'sortie' => $this->operation_sens == 'Sortie' ? $this->quantite : 0,
            'stock' => $this->quantiteApres
        ];
    }
}
