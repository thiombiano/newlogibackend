<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EtapeConstructionCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'libelleetape' => $this->libelleetape,
            'description' => $this->description,
            'montant_brut' => $this->montant_brut,
            'montant_macon' => $this->montant_macon,
            'montant_verse_entreprise' => $this->montant_verse_entreprise,
        ];
    }
}
