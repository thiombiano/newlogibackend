<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailReceptionCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'article' => $this->pivot->article,
            'reception' => $this->pivot->reception_id,
            'quantite' => $this->pivot->quantite,
            'commande' => $this->pivot->commande_id,
        ];
    }
}
