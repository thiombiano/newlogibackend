<?php

namespace App\Http\Resources;

use App\Models\BesoinJitem;
use App\Models\BesoinJournalier;
use App\Models\EtapeConstruction;
use App\Models\Maison;
use App\Models\Prefat;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class EnlevementItemCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'unite' => $this->unite_mesure->code,
            'article' => $this->libelle_article,
            'date_En' => $this->pivot->date_En,
            'article_id' => $this->id,
            'quantiteASortir' => $this->pivot->quantiteASortir,
            'maison_id' => $this->pivot->maison_id,
            'prefat_id' => $this->pivot->prefat_id,
            'etape_construction_id' => $this->pivot->etape_construction_id,
            'commentaires' => $this->pivot->commentaires,
            'besoin_jitems_id' => $this->pivot->besoin_jitems_id,
            'quantiteBesoin' => BesoinJitem::select('quantiteBesoin')->where('id', '=', $this->pivot->besoin_jitems_id)->pluck('quantiteBesoin')->first(),
            'libelleetape' => EtapeConstruction::select('libelleetape')->where('id', '=', $this->pivot->etape_construction_id)->pluck('libelleetape')->first(),
            'libellemaison' => Maison::select('libellemaison')->where('id', '=', $this->pivot->maison_id)->pluck('libellemaison')->first(),
            'libelleprefat' => Prefat::select('libelleprefat')->where('id', '=',  $this->pivot->prefat_id)->pluck('libelleprefat')->first(),
        ];
    }
}
