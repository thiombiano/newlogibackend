<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChantierReceptionsCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'numero_RC' => $this->numero_RC,
            'date_RC' => $this->date_RC,
            'statut_RC' => $this->statut_RC,
            'isValidateForReceptionPFinalle' => $this->isValidateForReceptionPFinalle,
            'fournisseur' => $this->fournisseur->raison_social,
            'fournisseur_id' => $this->fournisseur->id,
            'chantier' => $this->chantier->denomination,
            'chantier_id' => $this->chantier->id,
            'commande' => $this->commande->numero_BC,
            'commande_id' => $this->commande->id,
            'commentaires' => $this->commentaires,


        ];
    }
}
