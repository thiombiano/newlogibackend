<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RecueilItemCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'unite' => $this->unite_mesure->code,
            'article' => $this->libelle_article,
            'article_id' => $this->id,
            'quantiteDemandee' => $this->pivot->quantiteDemandee,
        ];
    }
}
