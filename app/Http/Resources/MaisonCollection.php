<?php

namespace App\Http\Resources;

use App\Models\Lot;
use App\Models\SocieteConstructive;
use App\Models\TypeMaison;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MaisonCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'libellemaison' => $this->libellemaison,
            'description' => $this->description,
            'type_maison_id' => $this->type_maison_id,
            'lot_id' => $this->lot_id,
            'technicien_id' => $this->technicien_id,
            'is_finished' => $this->is_finished,
            'societe_id' => $this->societe_id,
            'is_actif' => $this->is_actif,
            'controleur_id' => $this->controleur_id,
            'superficie' => $this->superficie,
            'codevirtuel' => $this->codevirtuel,
            'client' => $this->client,
            'lot' => LotMaisonCollection::collection(Lot::where('id','=', $this->lot_id)->get()),
            'societe' => SocieteCollection::collection(SocieteConstructive::where('id','=',$this->societe_id)->get()),
            'technicien' => UserCollection::collection(User::where('id','=',$this->technicien_id)->get()),
            'controleur' => UserCollection::collection(User::where('id','=',$this->controleur_id)->get()),
            'typemaison' => TypemaisonCollection::collection(TypeMaison::where('id','=',$this->type_maison_id)->get()),
        ];
    }
}
