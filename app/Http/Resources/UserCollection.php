<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'prenomemploye' => $this->prenomemploye,
            'telemploye' => $this->telemploye,
            'codeemploye' => $this->codeemploye,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'isActif' => $this->isActif,
            'login' => $this->login,
            'roles' =>  RoleCollection::collection($this->roles),
        ];
    }
}
