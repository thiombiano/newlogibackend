<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReceptionCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'numero_RC' => $this->numero_RC,
            'date_RC' => $this->date_RC,
            'statut_RC' => $this->statut_RC,
            'commande_id' => $this->commande_id,
            'numero_BC' => $this->commande->numero_BC,
            'commande' => $this->commande->numero_BC,
            'fournisseur_id' => $this->fournisseur_id,
            'fournisseur' => $this->fournisseur->raison_social,
            'chantier_id' => $this->commande->chantier_id,
            'typedereception' => $this->typedereception,
            'numero_piece_fournisseur' => $this->numero_piece_fournisseur,
            'isValidateForReceptionPFinalle' => $this->isValidateForReceptionPFinalle,
            'articles' => ReceptionItemCollection::collection($this->reception_items)
        ];
    }
}
