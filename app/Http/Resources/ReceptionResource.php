<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReceptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'numero_RC' => $this->numero_RC,
            'date_RC' => $this->date_RC,
            'fournisseur' => $this->commande->fournisseur->raison_social,
            'fournisseur_id' => $this->commande->fournisseur->id,
            'statut_RC' => $this->statut_RC,
            'commentaires' => $this->commentaires,
            'numero_piece_fournisseur' => $this->numero_piece_fournisseur,
            'livreur' => $this->livreur,
            'telephone_livreur' => $this->telephone_livreur,
            'isValidateForReceptionPFinalle' => $this->isValidateForReceptionPFinalle,
            'BC' => $this->commande->numero_BC,
            'commande_id' => $this->commande->id,
            'articles' => ReceptionItemCollection::collection($this->reception_items)
        ];
    }
}
