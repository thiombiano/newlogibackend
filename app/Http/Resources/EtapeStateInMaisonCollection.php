<?php

namespace App\Http\Resources;

use App\Models\EtapeConstruction;
use App\Models\Lot;
use App\Models\Maison;
use App\Models\SocieteConstructive;
use App\Models\TypeMaison;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EtapeStateInMaisonCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->datefermetureetape)
            $datedefin = Carbon::parse($this->datefermetureetape);
        else
            $datedefin = Carbon::parse($this->dateouvertureetape);
        $datededebut = Carbon::parse($this->dateouvertureetape);
        return [
            'id' => $this->id,
            'dateouvertureetape' => $this->dateouvertureetape,
            'datefermetureetape' => $this->datefermetureetape,
            'etape_construction_id' => $this->etape_construction_id,
            'maison_id' => $this->maison_id,
            'etapeState' => $this->etapeState,
            'isFinished' => $this->isFinished,
            'commentaires' => $this->commentaires,
            'nombredejour' => $datedefin->diffInDays($datededebut),
            'nombretotaletape' => $this->nombretotaletape,
            'numeroetapencour' => $this->numeroetapencour,
            'etape' => EtapeConstructionCollection::collection(EtapeConstruction::where('id', '=', $this->etape_construction_id)->get()),
            'maison' => MaisonCollection::collection(Maison::where('id','=',$this->maison_id)->get()),
        ];
    }
}
