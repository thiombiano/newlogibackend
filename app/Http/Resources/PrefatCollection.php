<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PrefatCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'libelleprefat' => $this->libelleprefat,
            'technicien_id' => $this->technicien_id,
            'magasinier_id' => $this->magasinier_id,
            'technicien' => UserCollection::collection(User::where('id','=',$this->technicien_id)->get()),
            'magasinier' => UserCollection::collection(User::where('id','=',$this->magasinier_id)->get()),
        ];
    }
}
