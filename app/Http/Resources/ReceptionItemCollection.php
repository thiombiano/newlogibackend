<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReceptionItemCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->article->reference,
            'article' => $this->article->libelle_article,
            'unite' => $this->article->unite_mesure->code,
            'quantite_receptionnee' => $this->quantite_receptionnee,
            'commentaires' => $this->commentaires,
        ];
    }
}
