<?php

namespace App\Http\Resources;

use App\Models\BesoinJournalier;
use App\Models\Livreur;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class EnlevementResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $technicien_id =  BesoinJournalier::select('technicien_id')->where('id', '=', $this->besoin_journalier_id)->pluck('technicien_id')->first();
        return [
            'id' => $this->id,
            'numero_En' => $this->numero_En,
            'date_En' => $this->date_En,
            'statut_En' => $this->statut_En,
            'typeenlevement' => $this->typeenlevement,
            'commentaires' => $this->commentaires,
            'chantier_id' => $this->chantier_id,
            'magasinier_id' => $this->magasinier_id,
            'livreur_id' => $this->livreur_id,
            'besoin_journalier_id' => $this->besoin_journalier_id,
            'nommagasinier' => User::where('id','=',$this->magasinier_id)->get()->pluck('name')->first(),
            'nomtechnicien' => User::where('id', '=', $technicien_id)->get()->pluck('name')->first(),
            'nomlivreur' => Livreur::where('id', '=',  $this->livreur_id)->get()->pluck('nomlivreur')->first(),
            'prenomlivreur' => Livreur::where('id', '=',  $this->livreur_id)->get()->pluck('prenomlivreur')->first(),
            'telephonelivreur' => Livreur::where('id','=',$this->livreur_id)->get()->pluck('telephonelivreur')->first(),
            'telephonemagasinier' => User::where('id','=',$this->magasinier_id)->get()->pluck('telemploye')->first(),
            'telephonetechnicien' => User::where('id', '=', $technicien_id)->get()->pluck('telemploye')->first(),
            'technicien_id' =>  BesoinJournalier::select('technicien_id')->where('id', '=', $this->besoin_journalier_id)->pluck('technicien_id')->first(),
            'besoin' => BesoinJournalierResource::collection(BesoinJournalier::where('id', '=', $this->besoin_journalier_id)->get()),
            'magasinier' => UserCollection::collection(User::where('id','=',  $this->magasinier_id)->get()),
            'articles' => EnlevementItemCollection::collection($this->articles)
        ];
    }
}
