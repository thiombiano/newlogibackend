<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RecueilBesoinCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'numeroRB' => $this->numeroRB,
            'chantier' => $this->chantier->denomination,
            'chantier_id' => $this->chantier->id,
            'date' => $this->dateRB,
            'dateLivraisonCible' => $this->dateLivraisonCible,
            'statutRB' => $this->statutRB,
            'href' => [
                'link' => route('recueilbesoins.show', $this->id)
            ]
        ];
    }
}
