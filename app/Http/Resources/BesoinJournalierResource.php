<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BesoinJournalierResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'journee_id' => $this->journee_id,
            'chantier_id' => $this->chantier_id,
            'cahierjournalier' => $this->cahierjournalier,
            'technicien_id' => $this->technicien_id,
            'dateBJ' => $this->dateBJ,
            'typebesoin'=> $this->typebesoin,
            'nomtechnicien' => User::where('id','=',$this->technicien_id)->get()->pluck('name')->first(),
            'technicien' => UserCollection::collection(User::where('id','=',$this->technicien_id)->get()),
            'articles' => BesoinItemJournalierCollection::collection($this->articles)
        ];
    }
}
