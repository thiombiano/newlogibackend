<?php

namespace App\Console;

use App\Models\Commande;
use App\Models\CommandeItem;
use App\Models\Reception;
use App\Models\ReceptionItem;
use App\Notifications\ReceptionNotification;
use App\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Notification;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

//        global $receptionpartielleexist;
//        $receptionpartielleexist  = 'Pas de receptions partielles en attente';
//        $reception_partielle = Reception::where('typedereception', '=', 'Reception partielle')->get();
//        foreach ($reception_partielle as $rp) {
//            $reception_items = ReceptionItem::where('reception_id', '=', $rp->id)->get();
//            $idcommande = Reception::select('commande_id')->where('id', '=', $rp->id)->pluck('commande_id')->first();
//            $commande = Commande::findOrFail($idcommande);
//
//            if($commande->is_receptionne == 0) {
//                $commande_items = CommandeItem::where('commande_id', '=', $idcommande)->get();
//                foreach ($commande_items as $detailc) {
//                    foreach ($reception_items as $detailr) {
//                        if($detailc->article_id == $detailr->article_id)
//                        {
//                            if($detailc->quantite_commandee != $detailr->quantite_receptionnee) {
//                                $receptionpartielleexist = 'Receptions partielles en attente';
//                                break;
//                            }
//                        }
//                    }
//                }
//            }
//        }

//        if($receptionpartielleexist == "Receptions partielles en attente") {

//            $schedule->call(function () {
//
//                $user = User::first();
//
//                $details = [
//
//                    'greeting' => 'Bonjour / Bonsoir M. '.$user->name,
//
//                    'body' => 'Notification vous avez des articles que vous n\' avez pas encore réceptionné veuiller vous connecter à CHANTI - GEST',
//
//                    'thanks' => 'Merci de vous connectez à la plateforme CHANTI-GEST pour traiter cette réception',
//
//                    'actionText' => 'Accéder à CHANTI-GEST',
//
//                    'actionURL' => url('http://10.100.35.2'),
//
//                    'order_id' => 101
//
//                ];
//                Notification::send($user, new ReceptionNotification($details));
//            })->dailyAt('15:43');

//        }

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
