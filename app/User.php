<?php

namespace App;

use App\Models\Chantier;
use App\Models\Maison;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\UserResolver;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject , UserResolver
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'email', 'password', 'codeemploye', 'telemploye', 'prenomemploye', 'isActif', 'realpassword', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
        'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($value){
        return $this->attributes['password'] = bcrypt($value);
    }

    public function roles(){
        return $this->belongsToMany(Role::class,'users_has_roles');
    }

    public function chantiers()
    {
        return $this->hasMany(Chantier::class);
    }

    public function maisons(){
        return $this->hasMany(Maison::class);
    }

    /**
     * Resolve the User.
     *
     * @return mixed|null
     */
    public static function resolve()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
